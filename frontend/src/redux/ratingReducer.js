// src/redux/ratingReducer.js

import {
    FETCH_AVERAGE_RATING_REQUEST,
    FETCH_AVERAGE_RATING_SUCCESS,
    FETCH_AVERAGE_RATING_FAILURE,
    CREATE_RATING_REQUEST,
    CREATE_RATING_SUCCESS,
    CREATE_RATING_FAILURE,
} from "./ratingActions";

const initialState = {
    averageRating: null,
    loading: false,
    error: null,
};

export const ratingReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_AVERAGE_RATING_REQUEST:
        case CREATE_RATING_REQUEST:
            return { ...state, loading: true, error: null };
        case FETCH_AVERAGE_RATING_SUCCESS:
            return {
                ...state,
                averageRating: action.payload.average_rating,
                loading: false,
            };
        case CREATE_RATING_SUCCESS:
            return { ...state, loading: false };
        case FETCH_AVERAGE_RATING_FAILURE:
        case CREATE_RATING_FAILURE:
            return { ...state, error: action.payload, loading: false };
        default:
            return state;
    }
};
