// src/redux/groupActions.js

import {
    fetchGroups,
    fetchGroupById,
    createGroup,
    updateGroup,
    deleteGroup,
    inviteMember,
    acceptInvite,
} from "../api/services/groupeService";

// src/redux/groupActionTypes.js

export const FETCH_GROUPS_REQUEST = 'FETCH_GROUPS_REQUEST';
export const FETCH_GROUPS_SUCCESS = 'FETCH_GROUPS_SUCCESS';
export const FETCH_GROUPS_FAILURE = 'FETCH_GROUPS_FAILURE';

export const FETCH_GROUP_REQUEST = 'FETCH_GROUP_REQUEST';
export const FETCH_GROUP_SUCCESS = 'FETCH_GROUP_SUCCESS';
export const FETCH_GROUP_FAILURE = 'FETCH_GROUP_FAILURE';

export const CREATE_GROUP_REQUEST = 'CREATE_GROUP_REQUEST';
export const CREATE_GROUP_SUCCESS = 'CREATE_GROUP_SUCCESS';
export const CREATE_GROUP_FAILURE = 'CREATE_GROUP_FAILURE';

export const UPDATE_GROUP_REQUEST = 'UPDATE_GROUP_REQUEST';
export const UPDATE_GROUP_SUCCESS = 'UPDATE_GROUP_SUCCESS';
export const UPDATE_GROUP_FAILURE = 'UPDATE_GROUP_FAILURE';

export const DELETE_GROUP_REQUEST = 'DELETE_GROUP_REQUEST';
export const DELETE_GROUP_SUCCESS = 'DELETE_GROUP_SUCCESS';
export const DELETE_GROUP_FAILURE = 'DELETE_GROUP_FAILURE';

export const INVITE_MEMBER_REQUEST = 'INVITE_MEMBER_REQUEST';
export const INVITE_MEMBER_SUCCESS = 'INVITE_MEMBER_SUCCESS';
export const INVITE_MEMBER_FAILURE = 'INVITE_MEMBER_FAILURE';

export const ACCEPT_INVITE_REQUEST = 'ACCEPT_INVITE_REQUEST';
export const ACCEPT_INVITE_SUCCESS = 'ACCEPT_INVITE_SUCCESS';
export const ACCEPT_INVITE_FAILURE = 'ACCEPT_INVITE_FAILURE';


// جلب المجموعات
export const getGroups = (page) => async (dispatch) => {
    dispatch({ type: FETCH_GROUPS_REQUEST });
    try {
        const response = await fetchGroups(page);
        dispatch({ type: FETCH_GROUPS_SUCCESS, payload: response.data });
    } catch (error) {
        dispatch({ type: FETCH_GROUPS_FAILURE, payload: error.message });
    }
};

// جلب مجموعة حسب المعرف
export const getGroupById = (id) => async (dispatch) => {
    dispatch({ type: FETCH_GROUP_REQUEST });
    try {
        const response = await fetchGroupById(id);
        dispatch({ type: FETCH_GROUP_SUCCESS, payload: response.data });
    } catch (error) {
        dispatch({ type: FETCH_GROUP_FAILURE, payload: error.message });
    }
};

// إنشاء مجموعة
export const createGroupAction = (groupData) => async (dispatch) => {
    dispatch({ type: CREATE_GROUP_REQUEST });
    try {
        const response = await createGroup(groupData);
        dispatch({ type: CREATE_GROUP_SUCCESS, payload: response.data });
    } catch (error) {
        dispatch({ type: CREATE_GROUP_FAILURE, payload: error.message });
    }
};

// تحديث مجموعة
export const updateGroupAction = (id, groupData) => async (dispatch) => {
    dispatch({ type: UPDATE_GROUP_REQUEST });
    try {
        const response = await updateGroup(id, groupData);
        dispatch({ type: UPDATE_GROUP_SUCCESS, payload: response.data });
    } catch (error) {
        dispatch({ type: UPDATE_GROUP_FAILURE, payload: error.message });
    }
};

// حذف مجموعة
export const deleteGroupAction = (id) => async (dispatch) => {
    dispatch({ type: DELETE_GROUP_REQUEST });
    try {
        await deleteGroup(id);
        dispatch({ type: DELETE_GROUP_SUCCESS, payload: id });
    } catch (error) {
        dispatch({ type: DELETE_GROUP_FAILURE, payload: error.message });
    }
};

// دعوة عضو
export const inviteMemberAction = (groupId, userId) => async (dispatch) => {
    dispatch({ type: INVITE_MEMBER_REQUEST });
    try {
        const response = await inviteMember(groupId, userId);
        dispatch({ type: INVITE_MEMBER_SUCCESS, payload: response.data });
    } catch (error) {
        dispatch({ type: INVITE_MEMBER_FAILURE, payload: error.message });
    }
};

// قبول الدعوة
export const acceptInviteAction = (groupId, userId) => async (dispatch) => {
    dispatch({ type: ACCEPT_INVITE_REQUEST });
    try {
        const response = await acceptInvite(groupId, userId);
        dispatch({ type: ACCEPT_INVITE_SUCCESS, payload: response.data });
    } catch (error) {
        dispatch({ type: ACCEPT_INVITE_FAILURE, payload: error.message });
    }
};
