import {
    FETCH_JOBS_REQUEST,
    FETCH_JOBS_SUCCESS,
    FETCH_JOBS_FAILURE,
    FETCH_JOB_REQUEST,
    FETCH_JOB_SUCCESS,
    FETCH_JOB_FAILURE,
    CREATE_JOB_REQUEST,
    CREATE_JOB_SUCCESS,
    CREATE_JOB_FAILURE,
    UPDATE_JOB_REQUEST,
    UPDATE_JOB_SUCCESS,
    UPDATE_JOB_FAILURE,
    DELETE_JOB_REQUEST,
    DELETE_JOB_SUCCESS,
    DELETE_JOB_FAILURE,
} from "./freeJobActions";

const initialState = {
    loading: false,
    jobs: [],
    job: {},
    error: null,
};

export const freeJobReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_JOBS_REQUEST:
        case FETCH_JOB_REQUEST:
        case CREATE_JOB_REQUEST:
        case UPDATE_JOB_REQUEST:
        case DELETE_JOB_REQUEST:
            return { ...state, loading: true, error: null };

        case FETCH_JOBS_SUCCESS:
            return { ...state, loading: false, jobs: action.payload };

        case FETCH_JOB_SUCCESS:
            return { ...state, loading: false, job: action.payload };

        case CREATE_JOB_SUCCESS:
            return {
                ...state,
                loading: false,
                jobs: [...state.jobs, action.payload],
            };

        case UPDATE_JOB_SUCCESS:
            return {
                ...state,
                loading: false,
                jobs: state.jobs.map((job) =>
                    job.id === action.payload.id ? action.payload : job
                ),
            };

        case DELETE_JOB_SUCCESS:
            return {
                ...state,
                loading: false,
                jobs: state.jobs.filter((job) => job.id !== action.payload),
            };

        case FETCH_JOBS_FAILURE:
        case FETCH_JOB_FAILURE:
        case CREATE_JOB_FAILURE:
        case UPDATE_JOB_FAILURE:
        case DELETE_JOB_FAILURE:
            return { ...state, loading: false, error: action.payload };

        default:
            return state;
    }
};
