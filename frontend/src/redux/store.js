

// src/redux/store.js

import { combineReducers, applyMiddleware, legacy_createStore } from "redux";
import {thunk} from "redux-thunk"; // تأكد من استيراد thunk بالشكل الصحيح
import { userReducer } from "./userReducer";
import { companyReducer } from "./companyReducer";
import { freeJobReducer } from "./freeJobReducer";
import { groupReducer } from "./groupReducer";
import { messageReducer } from "./messageReducer";
import { notificationReducer } from "./notificationReducer";
import { ratingReducer } from "./ratingReducer";

// دمج المقتطعات
const rootReducer = combineReducers({
    auth: userReducer,
    company: companyReducer,
    freeJob: freeJobReducer,
    group: groupReducer,
    message: messageReducer,
    notification: notificationReducer,
    rating: ratingReducer
});

// إنشاء المتجر
const store = legacy_createStore(rootReducer, applyMiddleware(thunk));

export default store;
