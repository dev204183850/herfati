// src/redux/notificationActions.js

import {
    fetchNotifications,
    getNotificationById,
    markNotificationAsRead,
} from "../api/services/notificationsService";

// src/redux/notificationActionTypes.js

export const FETCH_NOTIFICATIONS_REQUEST = 'FETCH_NOTIFICATIONS_REQUEST';
export const FETCH_NOTIFICATIONS_SUCCESS = 'FETCH_NOTIFICATIONS_SUCCESS';
export const FETCH_NOTIFICATIONS_FAILURE = 'FETCH_NOTIFICATIONS_FAILURE';

export const FETCH_NOTIFICATION_REQUEST = 'FETCH_NOTIFICATION_REQUEST';
export const FETCH_NOTIFICATION_SUCCESS = 'FETCH_NOTIFICATION_SUCCESS';
export const FETCH_NOTIFICATION_FAILURE = 'FETCH_NOTIFICATION_FAILURE';

export const MARK_NOTIFICATION_READ_REQUEST = 'MARK_NOTIFICATION_READ_REQUEST';
export const MARK_NOTIFICATION_READ_SUCCESS = 'MARK_NOTIFICATION_READ_SUCCESS';
export const MARK_NOTIFICATION_READ_FAILURE = 'MARK_NOTIFICATION_READ_FAILURE';


// جلب الإشعارات
export const getNotifications = (page) => async (dispatch) => {
    dispatch({ type: FETCH_NOTIFICATIONS_REQUEST });
    try {
        const response = await fetchNotifications(page);
        dispatch({ type: FETCH_NOTIFICATIONS_SUCCESS, payload: response.data });
    } catch (error) {
        dispatch({ type: FETCH_NOTIFICATIONS_FAILURE, payload: error.message });
        console.error(error);
    }
};

// جلب إشعار محدد
export const getNotification = (id) => async (dispatch) => {
    dispatch({ type: FETCH_NOTIFICATION_REQUEST });
    try {
        const response = await getNotificationById(id);
        dispatch({ type: FETCH_NOTIFICATION_SUCCESS, payload: response.data });
    } catch (error) {
        dispatch({ type: FETCH_NOTIFICATION_FAILURE, payload: error.message });
        console.error(error);
    }
};

// علامة قراءة الإشعار
export const markNotificationRead = (id) => async (dispatch) => {
    dispatch({ type: MARK_NOTIFICATION_READ_REQUEST });
    try {
        const response = await markNotificationAsRead(id);
        dispatch({
            type: MARK_NOTIFICATION_READ_SUCCESS,
            payload: response.data,
        });
    } catch (error) {
        dispatch({
            type: MARK_NOTIFICATION_READ_FAILURE,
            payload: error.message,
        });
        console.error(error);
    }
};
