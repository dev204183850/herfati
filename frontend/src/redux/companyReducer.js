import {
    FETCH_COMPANIES_REQUEST,
    FETCH_COMPANIES_SUCCESS,
    FETCH_COMPANIES_FAILURE,
    FETCH_COMPANY_REQUEST,
    FETCH_COMPANY_SUCCESS,
    FETCH_COMPANY_FAILURE,
    CREATE_COMPANY_REQUEST,
    CREATE_COMPANY_SUCCESS,
    CREATE_COMPANY_FAILURE,
    UPDATE_COMPANY_REQUEST,
    UPDATE_COMPANY_SUCCESS,
    UPDATE_COMPANY_FAILURE,
    DELETE_COMPANY_REQUEST,
    DELETE_COMPANY_SUCCESS,
    DELETE_COMPANY_FAILURE,
} from "./companyActions";

const initialState = {
    loading: false,
    companies: [],
    company: {},
    error: null,
};

export const companyReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COMPANIES_REQUEST:
        case FETCH_COMPANY_REQUEST:
        case CREATE_COMPANY_REQUEST:
        case UPDATE_COMPANY_REQUEST:
        case DELETE_COMPANY_REQUEST:
            return { ...state, loading: true, error: null };

        case FETCH_COMPANIES_SUCCESS:
            return { ...state, loading: false, companies: action.payload };

        case FETCH_COMPANY_SUCCESS:
            return { ...state, loading: false, company: action.payload };

        case CREATE_COMPANY_SUCCESS:
            return {
                ...state,
                loading: false,
                companies: [...state.companies, action.payload],
            };

        case UPDATE_COMPANY_SUCCESS:
            return {
                ...state,
                loading: false,
                companies: state.companies.map((company) =>
                    company.id === action.payload.id ? action.payload : company
                ),
            };

        case DELETE_COMPANY_SUCCESS:
            return {
                ...state,
                loading: false,
                companies: state.companies.filter(
                    (company) => company.id !== action.payload
                ),
            };

        case FETCH_COMPANIES_FAILURE:
        case FETCH_COMPANY_FAILURE:
        case CREATE_COMPANY_FAILURE:
        case UPDATE_COMPANY_FAILURE:
        case DELETE_COMPANY_FAILURE:
            return { ...state, loading: false, error: action.payload };

        default:
            return state;
    }
};
