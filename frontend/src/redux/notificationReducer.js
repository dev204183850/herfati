// src/redux/notificationReducer.js

import {
    FETCH_NOTIFICATIONS_REQUEST,
    FETCH_NOTIFICATIONS_SUCCESS,
    FETCH_NOTIFICATIONS_FAILURE,
    FETCH_NOTIFICATION_REQUEST,
    FETCH_NOTIFICATION_SUCCESS,
    FETCH_NOTIFICATION_FAILURE,
    MARK_NOTIFICATION_READ_REQUEST,
    MARK_NOTIFICATION_READ_SUCCESS,
    MARK_NOTIFICATION_READ_FAILURE,
} from "./notificationActions";

const initialState = {
    notifications: [],
    notification: {},
    loading: false,
    error: null,
};

export const notificationReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_NOTIFICATIONS_REQUEST:
        case FETCH_NOTIFICATION_REQUEST:
        case MARK_NOTIFICATION_READ_REQUEST:
            return { ...state, loading: true, error: null };

        case FETCH_NOTIFICATIONS_SUCCESS:
            return { ...state, notifications: action.payload, loading: false };
        case FETCH_NOTIFICATION_SUCCESS:
            return { ...state, notification: action.payload, loading: false };
        case MARK_NOTIFICATION_READ_SUCCESS:
            return {
                ...state,
                notifications: state.notifications.map((notification) =>
                    notification.id === action.payload.id
                        ? action.payload
                        : notification
                ),
                loading: false,
            };

        case FETCH_NOTIFICATIONS_FAILURE:
        case FETCH_NOTIFICATION_FAILURE:
        case MARK_NOTIFICATION_READ_FAILURE:
            return { ...state, error: action.payload, loading: false };

        default:
            return state;
    }
};
