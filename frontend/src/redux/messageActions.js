// src/redux/messageActions.js

import {
    fetchUsers,
    createMessage,
    fetchMessages,
} from "../api/services/messageService";

// src/redux/messageActionTypes.js

export const FETCH_USERS_REQUEST = 'FETCH_USERS_REQUEST';
export const FETCH_USERS_SUCCESS = 'FETCH_USERS_SUCCESS';
export const FETCH_USERS_FAILURE = 'FETCH_USERS_FAILURE';

export const CREATE_MESSAGE_REQUEST = 'CREATE_MESSAGE_REQUEST';
export const CREATE_MESSAGE_SUCCESS = 'CREATE_MESSAGE_SUCCESS';
export const CREATE_MESSAGE_FAILURE = 'CREATE_MESSAGE_FAILURE';

export const FETCH_MESSAGES_REQUEST = 'FETCH_MESSAGES_REQUEST';
export const FETCH_MESSAGES_SUCCESS = 'FETCH_MESSAGES_SUCCESS';
export const FETCH_MESSAGES_FAILURE = 'FETCH_MESSAGES_FAILURE';


// جلب المستخدمين
export const getUsers = (page) => async (dispatch) => {
    dispatch({ type: FETCH_USERS_REQUEST });
    try {
        const response = await fetchUsers(page);
        dispatch({ type: FETCH_USERS_SUCCESS, payload: response.data });
    } catch (error) {
        dispatch({ type: FETCH_USERS_FAILURE, payload: error.message });
        console.error(error);
    }
};

// إرسال رسالة
export const sendMessage = (messageData) => async (dispatch) => {
    dispatch({ type: CREATE_MESSAGE_REQUEST });
    try {
        const response = await createMessage(messageData);
        dispatch({ type: CREATE_MESSAGE_SUCCESS, payload: response.data });
    } catch (error) {
        dispatch({ type: CREATE_MESSAGE_FAILURE, payload: error.message });
        console.error(error);
    }
};

// جلب الرسائل
export const getMessages = (userId, page) => async (dispatch) => {
    dispatch({ type: FETCH_MESSAGES_REQUEST });
    try {
        const response = await fetchMessages(userId, page);
        dispatch({ type: FETCH_MESSAGES_SUCCESS, payload: response.data });
    } catch (error) {
        dispatch({ type: FETCH_MESSAGES_FAILURE, payload: error.message });
        console.error(error);
    }
};
