// src/redux/messageReducer.js

import {
    FETCH_USERS_REQUEST,
    FETCH_USERS_SUCCESS,
    FETCH_USERS_FAILURE,
    CREATE_MESSAGE_REQUEST,
    CREATE_MESSAGE_SUCCESS,
    CREATE_MESSAGE_FAILURE,
    FETCH_MESSAGES_REQUEST,
    FETCH_MESSAGES_SUCCESS,
    FETCH_MESSAGES_FAILURE,
} from "./messageActions";

const initialState = {
    loading: false,
    users: [],
    messages: [],
    error: null,
};

export const messageReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_USERS_REQUEST:
        case CREATE_MESSAGE_REQUEST:
        case FETCH_MESSAGES_REQUEST:
            return { ...state, loading: true, error: null };

        case FETCH_USERS_SUCCESS:
            return { ...state, users: action.payload, loading: false };
        case CREATE_MESSAGE_SUCCESS:
            return {
                ...state,
                messages: [...state.messages, action.payload],
                loading: false,
            };
        case FETCH_MESSAGES_SUCCESS:
            return { ...state, messages: action.payload, loading: false };

        case FETCH_USERS_FAILURE:
        case CREATE_MESSAGE_FAILURE:
        case FETCH_MESSAGES_FAILURE:
            return { ...state, error: action.payload, loading: false };

        default:
            return state;
    }
};
