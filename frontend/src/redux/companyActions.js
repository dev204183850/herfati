import {
    fetchCompanies,
    fetchCompanyById,
    createCompany,
    updateCompany,
    deleteCompany,
} from "../api/services/companiesService";

export const FETCH_COMPANIES_REQUEST = "FETCH_COMPANIES_REQUEST";
export const FETCH_COMPANIES_SUCCESS = "FETCH_COMPANIES_SUCCESS";
export const FETCH_COMPANIES_FAILURE = "FETCH_COMPANIES_FAILURE";

export const FETCH_COMPANY_REQUEST = "FETCH_COMPANY_REQUEST";
export const FETCH_COMPANY_SUCCESS = "FETCH_COMPANY_SUCCESS";
export const FETCH_COMPANY_FAILURE = "FETCH_COMPANY_FAILURE";

export const CREATE_COMPANY_REQUEST = "CREATE_COMPANY_REQUEST";
export const CREATE_COMPANY_SUCCESS = "CREATE_COMPANY_SUCCESS";
export const CREATE_COMPANY_FAILURE = "CREATE_COMPANY_FAILURE";

export const UPDATE_COMPANY_REQUEST = "UPDATE_COMPANY_REQUEST";
export const UPDATE_COMPANY_SUCCESS = "UPDATE_COMPANY_SUCCESS";
export const UPDATE_COMPANY_FAILURE = "UPDATE_COMPANY_FAILURE";

export const DELETE_COMPANY_REQUEST = "DELETE_COMPANY_REQUEST";
export const DELETE_COMPANY_SUCCESS = "DELETE_COMPANY_SUCCESS";
export const DELETE_COMPANY_FAILURE = "DELETE_COMPANY_FAILURE";


export const getCompanies = (page) => async (dispatch) => {
    dispatch({ type: FETCH_COMPANIES_REQUEST });
    try {
        const response = await fetchCompanies(page);
        dispatch({ type: FETCH_COMPANIES_SUCCESS, payload: response });
    } catch (error) {
        dispatch({ type: FETCH_COMPANIES_FAILURE, payload: error });
        console.error("Error fetching companies:", error);
    }
};

export const getCompanyById = (id) => async (dispatch) => {
    dispatch({ type: FETCH_COMPANY_REQUEST });
    try {
        const response = await fetchCompanyById(id);
        dispatch({ type: FETCH_COMPANY_SUCCESS, payload: response });
    } catch (error) {
        dispatch({ type: FETCH_COMPANY_FAILURE, payload: error });
        console.error("Error fetching company:", error);
    }
};

export const createCompanyAction = (companyData) => async (dispatch) => {
    dispatch({ type: CREATE_COMPANY_REQUEST });
    try {
        const response = await createCompany(companyData);
        dispatch({ type: CREATE_COMPANY_SUCCESS, payload: response });
        dispatch(getCompanies()); // جلب الشركات بعد الإضافة
    } catch (error) {
        dispatch({ type: CREATE_COMPANY_FAILURE, payload: error });
        console.error("Error creating company:", error);
    }
};

export const updateCompanyAction = (id, companyData) => async (dispatch) => {
    dispatch({ type: UPDATE_COMPANY_REQUEST });
    try {
        const response = await updateCompany(id, companyData);
        dispatch({ type: UPDATE_COMPANY_SUCCESS, payload: response });
        dispatch(getCompanies()); // جلب الشركات بعد التحديث
    } catch (error) {
        dispatch({ type: UPDATE_COMPANY_FAILURE, payload: error });
        console.error("Error updating company:", error);
    }
};

export const deleteCompanyAction = (id) => async (dispatch) => {
    dispatch({ type: DELETE_COMPANY_REQUEST });
    try {
        await deleteCompany(id);
        dispatch({ type: DELETE_COMPANY_SUCCESS, payload: id });
        dispatch(getCompanies()); // جلب الشركات بعد الحذف
    } catch (error) {
        dispatch({ type: DELETE_COMPANY_FAILURE, payload: error });
        console.error("Error deleting company:", error);
    }
};
