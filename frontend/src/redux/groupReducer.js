// src/redux/groupReducer.js

import {
    FETCH_GROUPS_REQUEST,
    FETCH_GROUPS_SUCCESS,
    FETCH_GROUPS_FAILURE,
    FETCH_GROUP_REQUEST,
    FETCH_GROUP_SUCCESS,
    FETCH_GROUP_FAILURE,
    CREATE_GROUP_REQUEST,
    CREATE_GROUP_SUCCESS,
    CREATE_GROUP_FAILURE,
    UPDATE_GROUP_REQUEST,
    UPDATE_GROUP_SUCCESS,
    UPDATE_GROUP_FAILURE,
    DELETE_GROUP_REQUEST,
    DELETE_GROUP_SUCCESS,
    DELETE_GROUP_FAILURE,
    INVITE_MEMBER_REQUEST,
    INVITE_MEMBER_SUCCESS,
    INVITE_MEMBER_FAILURE,
    ACCEPT_INVITE_REQUEST,
    ACCEPT_INVITE_SUCCESS,
    ACCEPT_INVITE_FAILURE,
} from "./groupActions";

const initialState = {
    loading: false,
    groups: [],
    group: {},
    error: null,
};

export const groupReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_GROUPS_REQUEST:
        case FETCH_GROUP_REQUEST:
        case CREATE_GROUP_REQUEST:
        case UPDATE_GROUP_REQUEST:
        case DELETE_GROUP_REQUEST:
        case INVITE_MEMBER_REQUEST:
        case ACCEPT_INVITE_REQUEST:
            return { ...state, loading: true, error: null };

        case FETCH_GROUPS_SUCCESS:
            return { ...state, groups: action.payload, loading: false };

        case FETCH_GROUP_SUCCESS:
            return { ...state, group: action.payload, loading: false };

        case CREATE_GROUP_SUCCESS:
            return {
                ...state,
                groups: [...state.groups, action.payload],
                loading: false,
            };

        case UPDATE_GROUP_SUCCESS:
            return {
                ...state,
                groups: state.groups.map((group) =>
                    group.id === action.payload.id ? action.payload : group
                ),
                loading: false,
            };

        case DELETE_GROUP_SUCCESS:
            return {
                ...state,
                groups: state.groups.filter(
                    (group) => group.id !== action.payload
                ),
                loading: false,
            };

        case INVITE_MEMBER_SUCCESS:
        case ACCEPT_INVITE_SUCCESS:
            return { ...state, loading: false };

        case FETCH_GROUPS_FAILURE:
        case FETCH_GROUP_FAILURE:
        case CREATE_GROUP_FAILURE:
        case UPDATE_GROUP_FAILURE:
        case DELETE_GROUP_FAILURE:
        case INVITE_MEMBER_FAILURE:
        case ACCEPT_INVITE_FAILURE:
            return { ...state, error: action.payload, loading: false };

        default:
            return state;
    }
};
