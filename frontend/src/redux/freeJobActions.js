import {
    fetchJobs,
    fetchJobById,
    createJob,
    updateJob,
    deleteJob,
} from "../api/services/freeJobsService";

// Actions for jobs
export const FETCH_JOBS_REQUEST = "FETCH_JOBS_REQUEST";
export const FETCH_JOBS_SUCCESS = "FETCH_JOBS_SUCCESS";
export const FETCH_JOBS_FAILURE = "FETCH_JOBS_FAILURE";

export const FETCH_JOB_REQUEST = "FETCH_JOB_REQUEST";
export const FETCH_JOB_SUCCESS = "FETCH_JOB_SUCCESS";
export const FETCH_JOB_FAILURE = "FETCH_JOB_FAILURE";

export const CREATE_JOB_REQUEST = "CREATE_JOB_REQUEST";
export const CREATE_JOB_SUCCESS = "CREATE_JOB_SUCCESS";
export const CREATE_JOB_FAILURE = "CREATE_JOB_FAILURE";

export const UPDATE_JOB_REQUEST = "UPDATE_JOB_REQUEST";
export const UPDATE_JOB_SUCCESS = "UPDATE_JOB_SUCCESS";
export const UPDATE_JOB_FAILURE = "UPDATE_JOB_FAILURE";

export const DELETE_JOB_REQUEST = "DELETE_JOB_REQUEST";
export const DELETE_JOB_SUCCESS = "DELETE_JOB_SUCCESS";
export const DELETE_JOB_FAILURE = "DELETE_JOB_FAILURE";


export const getJobs = (page) => async (dispatch) => {
    dispatch({ type: FETCH_JOBS_REQUEST });
    try {
        const response = await fetchJobs(page);
        dispatch({ type: FETCH_JOBS_SUCCESS, payload: response.data });
    } catch (error) {
        dispatch({ type: FETCH_JOBS_FAILURE, payload: error });
        console.error("Error fetching jobs:", error);
    }
};

export const getJobById = (id) => async (dispatch) => {
    dispatch({ type: FETCH_JOB_REQUEST });
    try {
        const response = await fetchJobById(id);
        dispatch({ type: FETCH_JOB_SUCCESS, payload: response.data });
    } catch (error) {
        dispatch({ type: FETCH_JOB_FAILURE, payload: error });
        console.error("Error fetching job:", error);
    }
};

export const createJobAction = (jobData) => async (dispatch) => {
    dispatch({ type: CREATE_JOB_REQUEST });
    try {
        const response = await createJob(jobData);
        dispatch({ type: CREATE_JOB_SUCCESS, payload: response.data });
        dispatch(getJobs()); // تحديث قائمة الوظائف بعد الإضافة
    } catch (error) {
        dispatch({ type: CREATE_JOB_FAILURE, payload: error });
        console.error("Error creating job:", error);
    }
};

export const updateJobAction = (id, jobData) => async (dispatch) => {
    dispatch({ type: UPDATE_JOB_REQUEST });
    try {
        const response = await updateJob(id, jobData);
        dispatch({ type: UPDATE_JOB_SUCCESS, payload: response.data });
        dispatch(getJobs()); // تحديث قائمة الوظائف بعد التحديث
    } catch (error) {
        dispatch({ type: UPDATE_JOB_FAILURE, payload: error });
        console.error("Error updating job:", error);
    }
};

export const deleteJobAction = (id) => async (dispatch) => {
    dispatch({ type: DELETE_JOB_REQUEST });
    try {
        await deleteJob(id);
        dispatch({ type: DELETE_JOB_SUCCESS, payload: id });
        dispatch(getJobs()); // تحديث قائمة الوظائف بعد الحذف
    } catch (error) {
        dispatch({ type: DELETE_JOB_FAILURE, payload: error });
        console.error("Error deleting job:", error);
    }
};
