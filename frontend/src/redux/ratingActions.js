// src/redux/ratingActions.js

import {
    fetchAverageRating,
    createRating,
} from "../api/services/ratingService";

// src/redux/ratingActionTypes.js

export const FETCH_AVERAGE_RATING_REQUEST = "FETCH_AVERAGE_RATING_REQUEST";
export const FETCH_AVERAGE_RATING_SUCCESS = "FETCH_AVERAGE_RATING_SUCCESS";
export const FETCH_AVERAGE_RATING_FAILURE = "FETCH_AVERAGE_RATING_FAILURE";

export const CREATE_RATING_REQUEST = "CREATE_RATING_REQUEST";
export const CREATE_RATING_SUCCESS = "CREATE_RATING_SUCCESS";
export const CREATE_RATING_FAILURE = "CREATE_RATING_FAILURE";


export const getAverageRating =
    (rateableId, rateableType) => async (dispatch) => {
        dispatch({ type: FETCH_AVERAGE_RATING_REQUEST });
        try {
            const response = await fetchAverageRating(rateableId, rateableType);
            dispatch({
                type: FETCH_AVERAGE_RATING_SUCCESS,
                payload: response.data,
            });
        } catch (error) {
            dispatch({
                type: FETCH_AVERAGE_RATING_FAILURE,
                payload: error.message,
            });
            console.error(error);
        }
    };

export const addRating = (ratingData) => async (dispatch) => {
    dispatch({ type: CREATE_RATING_REQUEST });
    try {
        const response = await createRating(ratingData);
        dispatch({ type: CREATE_RATING_SUCCESS, payload: response.data });
    } catch (error) {
        dispatch({ type: CREATE_RATING_FAILURE, payload: error.message });
        console.error(error);
    }
};
