import { useCallback } from "react";
import PropTypes from "prop-types";

const CardCompanie = ({ className = "" }) => {
  const onProperty1hoverContainerClick = useCallback(() => {
    // Please sync "Companies Detail" to the project
  }, []);

  return (
    <div
      style={{
        width: "408px",
        borderRadius: "5px",
        border: "1px dashed #9747ff",
        boxSizing: "border-box",
        maxWidth: "100%",
        height: "580px",
        overflow: "hidden",
        textAlign: "left",
        fontSize: "18px",
        color: "#18191c",
        fontFamily: "Inter",
      }}
      className={className}
    >
      <div
        style={{
          position: "absolute",
          top: "20px",
          left: "20px",
          boxShadow: "1px 3px 12px rgba(0, 0, 0, 0.75)",
          borderRadius: "8px",
          backgroundColor: "#fff",
          border: "1px solid #e4e5e8",
          boxSizing: "border-box",
          width: "368px",
          height: "260px",
          display: "flex",
          flexDirection: "column",
          alignItems: "flex-end",
          justifyContent: "flex-start",
          padding: "24px",
          gap: "16px",
        }}
      >
        <div
          style={{
            alignSelf: "stretch",
            overflow: "hidden",
            display: "flex",
            flexDirection: "column",
            alignItems: "flex-start",
            justifyContent: "flex-start",
            gap: "8px",
          }}
        >
          <div
            style={{
              alignSelf: "stretch",
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "flex-start",
              gap: "6px",
            }}
          >
            <div
              style={{
                alignSelf: "stretch",
                flex: "1",
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-start",
                justifyContent: "flex-start",
                gap: "4px",
              }}
            >
              <div
                style={{
                  alignSelf: "stretch",
                  height: "29px",
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <div
                  style={{
                    flex: "1",
                    position: "relative",
                    lineHeight: "28px",
                    fontWeight: "500",
                  }}
                >
                  Company Name
                </div>
              </div>
              <div
                style={{
                  alignSelf: "stretch",
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "flex-start",
                  justifyContent: "flex-start",
                  gap: "8px",
                  fontSize: "12px",
                  color: "#020202",
                }}
              >
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "flex-start",
                  }}
                >
                  <div
                    style={{
                      borderRadius: "3px",
                      backgroundColor: "rgba(64, 201, 29, 0.25)",
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "flex-start",
                      justifyContent: "flex-start",
                      padding: "4px 8px",
                    }}
                  >
                    <div
                      style={{
                        position: "relative",
                        lineHeight: "12px",
                        textTransform: "uppercase",
                        fontWeight: "600",
                      }}
                    >
                      Full-Time
                    </div>
                  </div>
                </div>
                <div
                  style={{
                    alignSelf: "stretch",
                    flex: "1",
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "flex-start",
                    gap: "4px",
                    fontSize: "14px",
                    color: "#767f8c",
                  }}
                >
                  <img
                    style={{
                      width: "18px",
                      position: "relative",
                      height: "18px",
                      overflow: "hidden",
                      flexShrink: "0",
                    }}
                    alt=""
                    src="/location-on4.svg"
                  />
                  <div
                    style={{
                      flex: "1",
                      position: "relative",
                      lineHeight: "20px",
                    }}
                  >
                    settat , el khier
                  </div>
                </div>
              </div>
            </div>
            <img
              style={{
                width: "74px",
                position: "relative",
                borderRadius: "4.08px",
                height: "74px",
                objectFit: "cover",
              }}
              alt=""
              src="/ellipse-800@2x.png"
            />
          </div>
          <div
            style={{
              alignSelf: "stretch",
              position: "relative",
              fontSize: "16px",
              lineHeight: "24px",
              color: "#5e6670",
              display: "inline-block",
              height: "72px",
              flexShrink: "0",
            }}
          >{`description aboit company smaltravail en void with HTML, JavaScript, CSS, PHP, Symphony and/or Laravel `}</div>
        </div>
        <div
          style={{
            alignSelf: "stretch",
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            fontSize: "16px",
            color: "#0a65cc",
          }}
        >
          <div
            style={{
              borderRadius: "3px",
              border: "1px solid #cee0f5",
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "flex-start",
              padding: "12px 24px",
            }}
          >
            <div
              style={{
                position: "relative",
                lineHeight: "24px",
                textTransform: "capitalize",
                fontWeight: "600",
              }}
            >
              show details
            </div>
          </div>
          <div
            style={{
              borderRadius: "84px",
              backgroundColor: "#ecf9ea",
              display: "flex",
              flexDirection: "row",
              alignItems: "flex-start",
              justifyContent: "flex-start",
              padding: "12px",
            }}
          >
            <img
              style={{
                width: "24px",
                position: "relative",
                height: "24px",
                overflow: "hidden",
                flexShrink: "0",
              }}
              alt=""
              src="/bookmark-border4.svg"
            />
          </div>
        </div>
      </div>
      <div
        style={{
          position: "absolute",
          top: "300px",
          left: "20px",
          boxShadow: "1px 3px 12px rgba(0, 0, 0, 0.75)",
          borderRadius: "8px",
          backgroundColor: "#fff",
          border: "2px solid rgba(39, 194, 0, 0.75)",
          boxSizing: "border-box",
          width: "368px",
          height: "260px",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "space-between",
          padding: "24px",
          cursor: "pointer",
          fontSize: "18.7px",
        }}
        onClick={onProperty1hoverContainerClick}
      >
        <div
          style={{
            width: "332.8px",
            overflow: "hidden",
            display: "flex",
            flexDirection: "column",
            alignItems: "flex-start",
            justifyContent: "flex-start",
            gap: "8.3px",
          }}
        >
          <div
            style={{
              alignSelf: "stretch",
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "flex-start",
              gap: "6.2px",
            }}
          >
            <div
              style={{
                alignSelf: "stretch",
                flex: "1",
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-start",
                justifyContent: "flex-start",
                gap: "4.2px",
              }}
            >
              <div
                style={{
                  alignSelf: "stretch",
                  height: "30.2px",
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <div
                  style={{
                    flex: "1",
                    position: "relative",
                    lineHeight: "29.12px",
                    fontWeight: "500",
                  }}
                >
                  Company Name
                </div>
              </div>
              <div
                style={{
                  alignSelf: "stretch",
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "flex-start",
                  justifyContent: "flex-start",
                  gap: "8.3px",
                  fontSize: "12.5px",
                  color: "#020202",
                }}
              >
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "flex-start",
                  }}
                >
                  <div
                    style={{
                      borderRadius: "3.12px",
                      backgroundColor: "rgba(64, 201, 29, 0.25)",
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "flex-start",
                      justifyContent: "flex-start",
                      padding: "4.2px 8.3px",
                    }}
                  >
                    <div
                      style={{
                        position: "relative",
                        lineHeight: "12.48px",
                        textTransform: "uppercase",
                        fontWeight: "600",
                      }}
                    >
                      Full-Time
                    </div>
                  </div>
                </div>
                <div
                  style={{
                    alignSelf: "stretch",
                    flex: "1",
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "flex-start",
                    gap: "4.2px",
                    fontSize: "14.6px",
                    color: "#767f8c",
                  }}
                >
                  <img
                    style={{
                      width: "18.7px",
                      position: "relative",
                      height: "18.7px",
                      overflow: "hidden",
                      flexShrink: "0",
                    }}
                    alt=""
                    src="/location-on5.svg"
                  />
                  <div
                    style={{
                      flex: "1",
                      position: "relative",
                      lineHeight: "20.8px",
                    }}
                  >
                    settat , el khier
                  </div>
                </div>
              </div>
            </div>
            <img
              style={{
                width: "77px",
                position: "relative",
                borderRadius: "4.24px",
                height: "77px",
                objectFit: "cover",
              }}
              alt=""
              src="/ellipse-800@2x.png"
            />
          </div>
          <div
            style={{
              alignSelf: "stretch",
              position: "relative",
              fontSize: "16.6px",
              lineHeight: "24.96px",
              color: "#5e6670",
              display: "inline-block",
              height: "74.9px",
              flexShrink: "0",
            }}
          >{`description aboit company smaltravail en void with HTML, JavaScript, CSS, PHP, Symphony and/or Laravel `}</div>
        </div>
        <div
          style={{
            alignSelf: "stretch",
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            fontSize: "16px",
            color: "#0a65cc",
          }}
        >
          <div
            style={{
              borderRadius: "3px",
              border: "1px solid #cee0f5",
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "flex-start",
              padding: "12px 24px",
            }}
          >
            <div
              style={{
                position: "relative",
                lineHeight: "24px",
                textTransform: "capitalize",
                fontWeight: "600",
              }}
            >
              show details
            </div>
          </div>
          <div
            style={{
              borderRadius: "84px",
              backgroundColor: "#ecf9ea",
              display: "flex",
              flexDirection: "row",
              alignItems: "flex-start",
              justifyContent: "flex-start",
              padding: "12px",
            }}
          >
            <img
              style={{
                width: "24px",
                position: "relative",
                height: "24px",
                overflow: "hidden",
                flexShrink: "0",
              }}
              alt=""
              src="/bookmark-border4.svg"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

CardCompanie.propTypes = {
  className: PropTypes.string,
};

export default CardCompanie;
