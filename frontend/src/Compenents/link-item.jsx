/* eslint-disable react/prop-types */
import { useState } from "react";
import './Animation.css';
const LinkItem = ({text}) => {
    const [isHovered, setIsHovered] = useState(false);

    return (
        <div
            onMouseEnter={() => setIsHovered(true)}
            onMouseLeave={() => setIsHovered(false)}
            style={{
                backgroundColor: isHovered ? "#F1F2F4" : "#F1F2F4",
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                gap: "8px",
                width: "auto",
                padding: "8px 0px",
                color: !isHovered ? "#424242" : "rgba(66, 66, 66, 0.75)",
            }}
        >
            <div
                style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                    justifyContent: "space-between",
                    position: "relative",
                }}
            >
                <div
                    style={{
                        position: "relative",
                        letterSpacing: "0.5px",
                        lineHeight: "24px",
                        fontWeight: "500",
                        zIndex: "0",
                    }}
                >
                    {text}
                </div>
                {isHovered && (
                    <div
                        style={{
                            position: "absolute",
                            top: "50%",
                            maxWidth: "100%",
                            overflow: "hidden",
                            maxHeight: "100%",
                            zIndex: "1",
                            width: "0",
                            height: "4px",
                            borderRadius: "0px",
                            marginTop: "8px",
                            borderTop: "2px solid #037DC2",
                            animation: "expandWidth 1s forwards",
                        }}
                    />
                )}
                {!isHovered && (
                    <div
                        style={{
                            position: "absolute",
                            top: "50%",
                            maxWidth: "100%",
                            overflow: "hidden",
                            maxHeight: "100%",
                            zIndex: "1",
                            width: "0",
                            height: "2px",
                            borderRadius: "0px",
                            marginTop: "8px",
                            borderTop: "2px solid #037DC2",
                            animation: "expandHeight 1s forwards",
                        }}
                    />
                )}
            </div>
        </div>
    );
};

export default LinkItem;
    //            <LinkItem text={"Home"} />;
