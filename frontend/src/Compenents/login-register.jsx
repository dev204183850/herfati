/* eslint-disable react/prop-types */
import { useEffect, useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import Button from "./button";
import InputComponent from "./input-form-login";
import RememberMe from "./remember-me";
import { useDispatch, useSelector } from "react-redux";
import { loginUser, registerUser } from "../redux/userAction";

const SignUpEtLogin = ({ variant }) => {
    const dispatch = useDispatch();
    const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);

    const location = useLocation();
    const path = location.pathname;
    const navigate = useNavigate();

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [payload, setPayload] = useState(null);
    const [requist, setRequist] = useState(false);

    const handleNameChange = (value) => setName(value);
    const handleEmailChange = (value) => setEmail(value);
    const handlePasswordChange = (value) => setPassword(value);
    const handleConfirmPasswordChange = (value) => setConfirmPassword(value);

    useEffect(() => {
        if (isLoggedIn === true) {
            navigate("/dashboard");
        }
    });
    useEffect(() => {
        if (path === "/login" && requist===true) {
            dispatch(loginUser(payload));
        } else if (path === "/register" && requist===true) {
            dispatch(registerUser(payload));
        }
    });

    const handleSubmit = async () => {
        path === "/login"
            ? setPayload({ email: email, password: password })
            : setPayload({
                  name: name,
                  email: email,
                  password: password,
                  password_confirmation: confirmPassword,
            });
        setRequist(true);
    };

    // دالة لإنشاء العنوان والنص التوضيحي
    const renderHeader = () => (
        <div
            style={{
                alignSelf: "stretch",
                position: "relative",
                height: "114px",
            }}
        >
            <b
                style={{
                    position: "absolute",
                    top: "calc(50% - 57px)",
                    left: "calc(50% - 234px)",
                    display: "inline-block",
                    width: "155px",
                    height: "42.4px",
                }}
            >
                {path === "/login" ? "Sign in" : "Sign up"}
            </b>
            <div
                style={{
                    position: "absolute",
                    top: "calc(50% + 6.1px)",
                    left: "calc(50% - 234px)",
                    width: "468px",
                    height: "50.9px",
                    fontSize: "16px",
                }}
            >
                <div
                    style={{
                        position: "absolute",
                        top: "calc(50% - 25.45px)",
                        left: "calc(50% - 234px)",
                        display: "inline-block",
                        width: "468px",
                        height: "22.6px",
                    }}
                >
                    {path === "/login"
                        ? "If you don’t have an account register"
                        : "If you already have an account, Sign in"}
                </div>
                <div
                    style={{
                        position: "absolute",
                        top: "calc(50% + 2.85px)",
                        left: "calc(50% - 234px)",
                        display: "inline-block",
                        width: "468px",
                        height: "22.6px",
                    }}
                >
                    <span style={{ whiteSpace: "pre-wrap" }}>
                        {`You can   `}
                    </span>

                    <b style={{ color: "#f79b2d" }}>
                        <Link to={path === "/login" ? "/register" : "/login"}>
                            <b>
                                {path === "/login"
                                    ? "Register here!"
                                    : "Sign In Here!"}
                            </b>
                        </Link>
                    </b>
                </div>
            </div>
        </div>
    );

    // دالة لإنشاء حقل الإدخال
    const renderInputFields = () => (
        <div
            style={{
                height: "auto",
                width: "auto",
                display: "flex",
                flexDirection: "column",
                justifyContent: "space-between",
                boxSizing: "border-box",
                alignItems: "flex-start",
            }}
        >
            {path === "/login" ? (
                <>
                    <InputComponent
                        label="Enter your Email"
                        leftIconSrc={"../public/mail-outline.svg"}
                        showLeftIcon={true}
                        showRightIcon={false}
                        onValueChange={handleEmailChange}
                    />
                    <InputComponent
                        label="Enter your password"
                        leftIconSrc={"../public/vpn-key.svg"}
                        showLeftIcon={true}
                        showRightIcon={true}
                        onValueChange={handlePasswordChange}
                    />
                </>
            ) : (
                <>
                    <InputComponent
                        label="Enter your Name"
                        leftIconSrc={"../public/account-circle.svg"}
                        showLeftIcon={true}
                        showRightIcon={false}
                        onValueChange={handleNameChange}
                    />
                    <InputComponent
                        label="Enter your Email"
                        leftIconSrc={"../public/mail-outline.svg"}
                        showLeftIcon={true}
                        showRightIcon={false}
                        onValueChange={handleEmailChange}
                    />
                    <InputComponent
                        label="Enter your password"
                        leftIconSrc={"../public/vpn-key.svg"}
                        showLeftIcon={true}
                        showRightIcon={true}
                        onValueChange={handlePasswordChange}
                    />
                    <InputComponent
                        label="Confirm your password"
                        leftIconSrc={"../public/vpn-key.svg"}
                        showLeftIcon={true}
                        showRightIcon={true}
                        onValueChange={handleConfirmPasswordChange}
                    />
                </>
            )}
        </div>
    );

    // دالة لإنشاء خيارات تسجيل الدخول بواسطة وسائل التواصل الاجتماعي
    const renderSocialLoginOptions = () => (
        <div
            style={{
                position: "absolute",
                top: variant !== 12 ? "calc(50% + 255px)" : "calc(50% + 230px)",
                left:
                    variant !== 12
                        ? "calc(50% - 90.1px)"
                        : "calc(50% - 90.1px)",
                width: "162.3px",
                height: "45.9px",
            }}
        >
            <div
                style={{
                    position: "absolute",
                    top: "calc(50% - 22.95px)",
                    left: "calc(50% - 81.15px)",
                    width: "162.3px",
                    height: "45.9px",
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "flex-start",
                    justifyContent: "space-between",
                }}
            >
                <img
                    style={{
                        width: "45.9px",
                        position: "relative",
                        height: "45.9px",
                    }}
                    alt=""
                    src="/facebook.svg"
                />
                <img
                    style={{
                        width: "45.9px",
                        position: "relative",
                        height: "45.9px",
                        overflow: "hidden",
                        flexShrink: "0",
                    }}
                    alt=""
                    src="/apple.svg"
                />
                <img
                    style={{
                        width: "45.9px",
                        position: "relative",
                        height: "45.9px",
                        overflow: "hidden",
                        flexShrink: "0",
                    }}
                    alt=""
                    src="/google.svg"
                />
            </div>
        </div>
    );

    // دالة لإنشاء زر الإغلاق
    const renderCloseButton = () => (
        <button
            style={{
                cursor: "pointer",
                border: "none",
                padding: "0",
                backgroundColor: "transparent",
                position: "absolute",
                top: "17px",
                right: "16px",
                width: "40px",
                height: "40px",
            }}
        >
            <Link to="/">
                <div
                    style={{
                        position: "absolute",
                        top: "0px",
                        right: "0px",
                        borderRadius: "0px 12px 0px 12px",
                        backgroundColor: "rgba(0, 0, 0, 0.75)",
                        width: "40px",
                        height: "40px",
                    }}
                />

                <img
                    style={{
                        position: "absolute",
                        top: "8.8px",
                        right: "8.6px",
                        width: "22.6px",
                        height: "22.6px",
                        objectFit: "contain",
                    }}
                    alt=""
                    src="/line-3.svg"
                />
                <img
                    style={{
                        position: "absolute",
                        top: "8.8px",
                        right: "8.6px",
                        width: "22.6px",
                        height: "22.6px",
                        objectFit: "contain",
                    }}
                    alt=""
                    src="/line-4.svg"
                />
            </Link>
        </button>
    );

    // دالة لإنشاء التذكير وكلمة المرور
    const renderRememberMeAndPassword = () => (
        <div
            style={{
                width: "429px",
                display: "flex",
                flexDirection: "row",
                alignItems: "flex-start",
                marginBottom: "20px",
                justifyContent: "space-between",
            }}
        >
            <RememberMe
                text={
                    path === "/login"
                        ? "Remember me"
                        : "Accept Conditions & Terms"
                }
            />
            <div
                style={{
                    width: "108px",
                    position: "relative",
                    fontWeight: "300",
                    display: "inline-block",
                    flexShrink: "0",
                }}
            >
                Forgot Password ?
            </div>
        </div>
    );

    // الإصدار الأول
    const renderVariant1 = () => (
        <div
            style={{
                width: "500px",
                position: "relative",
                borderRadius: "25px",
                height: "600px",
                overflow: "hidden",
                flexShrink: "0",
                backgroundImage: "url('/statesign-in@3x.png')",
                backgroundSize: "cover",
                backgroundRepeat: "no-repeat",
                backgroundPosition: "top",
            }}
        >
            <div
                style={{
                    position: "absolute",
                    top: "0px",
                    left: "calc(50% - 250px)",
                    width: "500px",
                    height: "600px",
                }}
            >
                <div
                    style={{
                        position: "absolute",
                        top: "0px",
                        left: "calc(50% - 250px)",
                        width: "500px",
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "center",
                        justifyContent: "flex-start",
                        padding: "16px",
                        boxSizing: "border-box",
                        gap: "32px",
                    }}
                >
                    {renderHeader()}
                    <div
                        style={{
                            alignSelf: "stretch",
                            display: "flex",
                            flexDirection: "column",
                            alignItems: "center",
                            justifyContent: "flex-start",
                            gap: "16px",
                            fontSize: "12px",
                        }}
                    >
                        {renderInputFields()}
                        {renderRememberMeAndPassword()}
                        <Button
                            size={"medium"} //
                            type={"primary"} //secondary autre
                            icon={"none"} //left
                            disabled={false}
                            text={"Login"}
                            onClick={handleSubmit}
                        />
                    </div>
                </div>
                <div
                    style={{
                        position: "absolute",
                        top: "479px",
                        left: "calc(50% - 65px)",
                        fontSize: "16px",
                        fontWeight: "500",
                        color: "#263238",
                        display: "inline-block",
                        width: "129.7px",
                        height: "22.6px",
                    }}
                >
                    or continue with
                </div>
                {renderSocialLoginOptions()}
            </div>
            {renderCloseButton()}
        </div>
    );

    // الإصدار الثاني
    const renderVariant2 = () => (
        <div
            style={{
                width: "500px",
                position: "relative",

                borderRadius: "25px",
                height: "600px",
                overflow: "hidden",
                flexShrink: "0",
                backgroundImage: "url('/statesign-in@3x.png')",
                backgroundSize: "cover",
                backgroundRepeat: "no-repeat",
                backgroundPosition: "top",
            }}
        >
            <div
                style={{
                    position: "absolute",
                    top: "0px",
                    left: "calc(50% - 250px)",
                    width: "500px",
                    height: "600px",
                }}
            >
                <div
                    style={{
                        position: "absolute",
                        top: "0px",
                        left: "calc(50% - 250px)",
                        width: "500px",
                        height: "514px",
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "center",
                        justifyContent: "flex-start",
                        padding: "16px",
                        boxSizing: "border-box",
                        gap: "32px",
                    }}
                >
                    {renderHeader()}
                    <div
                        style={{
                            alignSelf: "stretch",
                            height: "347px",
                            display: "flex",
                            flexDirection: "column",
                            alignItems: "center",
                            justifyContent: "flex-start",
                            gap: "16px",
                            fontSize: "12px",
                        }}
                    >
                        <div
                            style={{
                                height: "259px",
                                display: "flex",
                                flexDirection: "column",
                                alignItems: "center",
                                justifyContent: "space-between",
                            }}
                        >
                            {renderInputFields()}
                        </div>
                        {renderRememberMeAndPassword()}
                        <Button
                            size={"medium"} //
                            type={"primary"} //secondary autre
                            icon={"none"} //left
                            disabled={false}
                            text={"Register"}
                            onClick={handleSubmit}
                        />
                    </div>
                </div>
                <div
                    style={{
                        position: "absolute",
                        top: "529px",
                        left: "calc(50% - 65px)",
                        fontSize: "16px",
                        fontWeight: "500",
                        color: "#263238",
                        display: "inline-block",
                        width: "129.7px",
                        height: "22.6px",
                    }}
                >
                    or continue with
                </div>
                {renderSocialLoginOptions()}
            </div>
            {renderCloseButton()}
        </div>
    );

    // الإصدار المطلوب
    return path === "/login" ? renderVariant1() : renderVariant2();
};

export default SignUpEtLogin;
