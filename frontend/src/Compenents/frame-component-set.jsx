import { useState, useEffect } from "react";
import FrameComponent from "./frame-component";

const FrameComponentSet = () => {
  const [currentStep, setCurrentStep] = useState(1);

  const handleNext = () => {
    setCurrentStep((prevStep) => (prevStep < 4 ? prevStep + 1 : prevStep));
    console.log(currentStep);
  };

  const handlePrevious = () => {
    setCurrentStep((prevStep) => (prevStep > 1 ? prevStep - 1 : prevStep));
    console.log(currentStep);
  };

  useEffect(() => {
    console.log(currentStep);
  }, [currentStep]);

  return (
    <div
      style={{
        maxWidth: "100%",
        overflow: "hidden",
        display: "flex",
        flexDirection: "column",
        alignItems: "flex-start",
        justifyContent: "flex-start",
        padding: "20px 11px",
        gap: "20px",
        textAlign: "center",
        fontSize: "16px",
        color: "#fff",
        fontFamily: "Roboto",
      }}
    >
      <div
        style={{
          width: "680px",
          backgroundColor: "#fff",
          display: "flex",
          flexDirection: "column",
          alignItems: "flex-start",
          justifyContent: "center",
          gap: "8px",
        }}
      >
        <div
          style={{
            width: "680px",
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <div
            style={{
              width: "32px",
              borderRadius: "40px",
              backgroundColor: "#74a66f",
              height: "32px",
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <div
              style={{
                position: "relative",
                letterSpacing: "0.5px",
                lineHeight: "24px",
              }}
            >
              1
            </div>
          </div>
          <div
            style={{
              width: "32px",
              borderRadius: "40px",
              backgroundColor: currentStep >= 2 ? "#74a66f" : "#9e9e9e",
              height: "32px",
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <div
              style={{
                position: "relative",
                letterSpacing: "0.5px",
                lineHeight: "24px",
              }}
            >
              2
            </div>
          </div>
          <div
            style={{
              width: "32px",
              borderRadius: "40px",
              backgroundColor: currentStep >= 3 ? "#74a66f" : "#9e9e9e",
              height: "32px",
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <div
              style={{
                position: "relative",
                letterSpacing: "0.5px",
                lineHeight: "24px",
              }}
            >
              3
            </div>
          </div>
          <div
            style={{
              width: "32px",
              borderRadius: "40px",
              backgroundColor: currentStep >= 4 ? "#74a66f" : "#9e9e9e",
              height: "32px",
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <div
              style={{
                position: "relative",
                letterSpacing: "0.5px",
                lineHeight: "24px",
              }}
            >
              4
            </div>
          </div>
        </div>
        <div
          style={{
            width: `${(currentStep - 1) * 236}px`,
            position: "relative",
            borderRadius: "8px",
            backgroundColor: "#0275b1",
            height: "8px",
          }}
        />
        <FrameComponent
          handleNext={handleNext}
          handlePrevious={handlePrevious}
        />
      </div>
    </div>
  );
};

export default FrameComponentSet;
