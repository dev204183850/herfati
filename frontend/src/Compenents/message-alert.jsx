/* eslint-disable react/prop-types */
export const MessageAlert = ({ imageSrc, message, time, isJobAlert }) => (
    <div
        style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "flex-start",
            justifyContent: "flex-start",
            padding: "12px 24px",
            gap: "12px",
            zIndex: "9999999",
        }}
    >
        <img
            style={{
                width: isJobAlert ? "24px" : "48px",
                height: isJobAlert ? "24px" : "48px",
                borderRadius: isJobAlert ? "0" : "50%",
                objectFit: "cover",
            }}
            alt=""
            src={imageSrc}
        />
        <div
            style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-start",
                justifyContent: "flex-start",
                gap: "4px",
            }}
        >
            <div
                style={{
                    width: "316px",
                    lineHeight: "24px",
                    display: "inline-block",
                    color: "#000",
                }}
            >
                {message}
            </div>
            <div
                style={{
                    width: "316px",
                    fontSize: "14px",
                    lineHeight: "20px",
                    color: "#000",
                    display: "inline-block",
                }}
            >
                {time}
            </div>
        </div>
    </div>
);
