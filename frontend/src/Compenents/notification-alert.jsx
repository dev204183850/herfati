/* eslint-disable react/prop-types */

const NotificationItem = ({ text, time, onAccept, onReject }) => (
    <div
        style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "flex-start",
            justifyContent: "flex-start",
            padding: "12px 24px",
            gap: "12px",
            borderBottom: "1px solid #e4e5e8",
            width: "100%",
              zIndex:"9999999"
        }}
    >
        <div
            style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-start",
                justifyContent: "flex-start",
                gap: "4px",
                width: "100%",
            }}
        >
            <div
                style={{
                    width: "100%",
                    lineHeight: "24px",
                    display: "inline-block",
                    color:"#000"
                }}
            >
                {text}
            </div>
            <div
                style={{
                    fontSize: "14px",
                    lineHeight: "20px",
                    color: "#767f8c",
                    display: "inline-block",
                }}
            >
                {time}
            </div>
        </div>
        <div
            style={{
                display: "flex",
                gap: "10px",
            }}
        >
            <button
                onClick={onAccept}
                style={{
                    padding: "8px 16px",
                    backgroundColor: "#4CAF50",
                    color: "white",
                    border: "none",
                    borderRadius: "4px",
                    cursor: "pointer",
                }}
            >
                قبول
            </button>
            <button
                onClick={onReject}
                style={{
                    padding: "8px 16px",
                    backgroundColor: "#F44336",
                    color: "white",
                    border: "none",
                    borderRadius: "4px",
                    cursor: "pointer",
                }}
            >
                رفض
            </button>
        </div>
    </div>
);

export default NotificationItem;
