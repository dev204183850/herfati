import { Link } from "react-router-dom";

const Logo = () => {
    return (
        <Link to={"/"}>
            <div
                style={{
                    width: "117.5px",
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center",
                    position: "relative",
                    gap: "4px",
                }}
            >
                <img
                    style={{
                        width: "40px",
                        position: "relative",
                        height: "40px",
                        objectFit: "cover",
                        zIndex: "0",
                    }}
                    alt=""
                    src="/my-logo@2x.png"
                />
                <div
                    style={{
                        flex: "1",
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "center",
                        justifyContent: "center",
                        zIndex: "1",
                    }}
                >
                    <div
                        style={{
                            width: "73.5px",
                            height: "44.2px",
                            display: "flex",
                            flexDirection: "column",
                            alignItems: "center",
                            justifyContent: "flex-end",
                        }}
                    >
                        <div
                            style={{
                                display: "flex",
                                flexDirection: "column",
                                alignItems: "center",
                                justifyContent: "center",
                                padding: "0px 2px",
                                position: "relative",
                                gap: "1px",
                            }}
                        >
                            <img
                                style={{
                                    width: "67.4px",
                                    position: "relative",
                                    height: "20.7px",
                                    zIndex: "0",
                                }}
                                alt=""
                                src="/group.svg"
                            />
                            <img
                                style={{
                                    width: "4px",
                                    position: "absolute",
                                    margin: "0",
                                    top: "-6px",
                                    right: "1.4px",
                                    height: "4px",
                                    zIndex: "1",
                                }}
                                alt=""
                                src="/vector.svg"
                            />
                        </div>
                        <div
                            style={{
                                alignSelf: "stretch",
                                height: "17px",
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "center",
                                justifyContent: "center",
                                gap: "4px",
                            }}
                        >
                            <img
                                style={{
                                    width: "18.4px",
                                    position: "relative",
                                    height: "1px",
                                }}
                                alt=""
                                src="/vector1.svg"
                            />
                            <div
                                style={{
                                    display: "flex",
                                    flexDirection: "column",
                                    alignItems: "center",
                                    justifyContent: "center",
                                }}
                            >
                                <img
                                    style={{
                                        width: "24.8px",
                                        position: "relative",
                                        height: "12.1px",
                                    }}
                                    alt=""
                                    src="/group1.svg"
                                />
                            </div>
                            <img
                                style={{
                                    width: "18.4px",
                                    position: "relative",
                                    height: "1px",
                                }}
                                alt=""
                                src="/vector2.svg"
                            />
                        </div>
                    </div>
                </div>
                <img
                    style={{
                        width: "11.8px",
                        position: "absolute",
                        margin: "0",
                        top: "10.9px",
                        left: "8.2px",
                        height: "23.6px",
                        zIndex: "2",
                    }}
                    alt=""
                    src="/ellipse-15.svg"
                />
            </div>
        </Link>
    );
};
   // <Logo />;
export default Logo;
