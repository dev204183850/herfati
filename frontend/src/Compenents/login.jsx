import { useState } from 'react';
import { axiosInstance } from '../api/axios';
import { useNavigate } from "react-router-dom";


const Login = () => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const navigate = useNavigate(); // استخدم useNavigate

    const handleSubmit = async (event) => {
        event.preventDefault();
        // Get CSRF token
        await axiosInstance.get("/sanctum/csrf-cookie");
        // Post login data
        console.log("Get CSRF token");
        const values = { email: email, password: password };
        await axiosInstance.post("/login", values);
        window.localStorage.setItem('Acces_Token', 'auth');


        navigate("/dashboard");
        //console.log('Login attempted with:', { email, password });
    };

    return (
        <div
            style={{
                maxWidth: "400px",
                margin: "auto",
                padding: "20px",
                border: "1px solid #ccc",
            }}
        >
            <h2>Login</h2>
            <form onSubmit={handleSubmit}>
                <div style={{ marginBottom: "15px" }}>
                    <label
                        htmlFor="email"
                        style={{ display: "block", marginBottom: "5px" }}
                    >
                        Email:
                    </label>
                    <input
                        type="email"
                        id="email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required
                        style={{
                            width: "100%",
                            padding: "8px",
                            boxSizing: "border-box",
                        }}
                    />
                </div>
                <div style={{ marginBottom: "15px" }}>
                    <label
                        htmlFor="password"
                        style={{ display: "block", marginBottom: "5px" }}
                    >
                        Password:
                    </label>
                    <input
                        type="password"
                        id="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required
                        style={{
                            width: "100%",
                            padding: "8px",
                            boxSizing: "border-box",
                        }}
                    />
                </div>
                <button
                    type="submit"
                    style={{
                        width: "100%",
                        padding: "10px",
                        backgroundColor: "#007BFF",
                        color: "#fff",
                        border: "none",
                        cursor: "pointer",
                    }}
                >
                    Login
                </button>
            </form>
        </div>
    );
};

export default Login;
