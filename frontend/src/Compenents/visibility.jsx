/* eslint-disable react/prop-types */
import { useState } from "react";

const Visibility = ({ onVisibilityChange }) => {
    const [visibility, setVisibility] = useState(false);

    const handleVisibilityToggle = () => {
        setVisibility((prev) => {
            const newVisibility = !prev;
            onVisibilityChange(newVisibility);
            return newVisibility;
        });
    };

    return (
        <div>
            <img
                style={{
                    left: "20px",
                    width: "24px",
                    height: "24px",
                    cursor: "pointer",
                }}
                alt="visibility toggle"
                src={visibility ? "/property-1show.svg" : "/visibility.svg"}
                onClick={handleVisibilityToggle}
            />
        </div>
    );
};

export default Visibility;
