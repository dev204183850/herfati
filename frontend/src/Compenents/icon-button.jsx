/* eslint-disable react/prop-types */
import { useState } from "react";

const IconButton = ({ circle, icon, position }) => {
  const [hovering, setHovering] = useState(false);

  const handleMouseEnter = () => {
    setHovering(true);
  };

  const handleMouseLeave = () => {
    setHovering(false);
  };

  return (
    <div
      style={{
        width: "48px",
        borderRadius: circle ? "84px" : "5px",
        boxSizing: "border-box",
        height: "48px",
        overflow: "hidden",
      }}
    >
      <div
        style={{
          position: "absolute",
          top: "0px",
          left: "0px",
          borderRadius: circle ? "48px" : "5px",
          backgroundColor:
            position === "primary"
              ? hovering
                ? "#0056b3"
                : "#0a65cc"
              : position === "gray"
              ? hovering
                ? "#18191c"
                : "#666666"
              : position === "success"
              ? hovering
                ? "#1f7a1f"
                : "#27c200"
              : position === "warning"
              ? hovering
                ? "#ffaa00"
                : "#ffedcc"
              : position === "danger"
              ? hovering
                ? "#b30000"
                : "#ff4f4f"
              : "",
          display: "flex",
          flexDirection: "row",
          alignItems: "flex-start",
          justifyContent: "flex-start",
          padding: "12px",
        }}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
      >
        <img
          style={{
            width: "24px",
            position: "relative",
            height: "24px",
            overflow: "hidden",
            flexShrink: "0",
          }}
          alt=""
          src={icon}
        />
      </div>
    </div>
  );
};

export default IconButton;
//<IconButton position={"success"} icon={"arrow-forward.svg"} circle={true} />
