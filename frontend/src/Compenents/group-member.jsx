/* eslint-disable react/prop-types */
// src/components/GroupMemberList.js

import { useEffect, useState } from "react";
import { getGroupMembersByGroupId } from "../api/groupmember";

const GroupMemberList = ({ groupId }) => {
    const [groupMembers, setGroupMembers] = useState([]);

    useEffect(() => {
        const fetchGroupMembers = async () => {
            const response = await getGroupMembersByGroupId(groupId);
            setGroupMembers(response.data);
        };

        fetchGroupMembers();
    }, [groupId]);

    return (
        <div>
            <h1>Group Members</h1>
            <ul>
                {groupMembers.map((member) => (
                    <li key={member.id}>
                        {member.user_id} - {member.role}
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default GroupMemberList;
