import  { useState, useEffect, useMemo } from "react";

const Title = () => {
  const values = useMemo(() => [0, -75, -140, -205], []); // Wrap values initialization in useMemo

  const [index, setIndex] = useState(0);
  const [topValue, setTopValue] = useState(values[index]);

  useEffect(() => {
    const intervalId = setInterval(() => {
      setIndex((prevIndex) => (prevIndex + 1) % values.length);
    }, 1500); // 500ms توقف + 1000ms حركة

    return () => clearInterval(intervalId);
  }, [values.length]);

  useEffect(() => {
    setTopValue(values[index]);
  }, [index, values]);

  return (
      <div
          style={{
              width: "1004px",
              borderRadius: "5px",
              boxSizing: "border-box",
              maxWidth: "100%",
              display: "flex",
              flexDirection: "column",
              alignItems: "flex-start",
              justifyContent: "flex-start",
              gap: "100px",
              textAlign: "left",
              fontSize: "56px",
              color: "#000",
              fontFamily: "Inter",
              zIndex: "3",
          }}
      >
          <div
              style={{
                  width: "964px",
                  height: "136px",
                  flexShrink: "0",
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "flex-start",
                  position: "relative",
                  gap: "2px",
                  cursor: "pointer",
                  zIndex: "4",
              }}
          >
              <div
                  style={{
                      width: "964px",
                      position: "relative",
                      fontWeight: "500",
                      display: "inline-block",
                      flexShrink: "0",
                      zIndex: "5",
                  }}
              >
                  La Voie Directe vers Votre Prochain Emploi -
              </div>

              <div
                  style={{
                      width: "644px",
                      position: "absolute",
                      margin: "0",
                      top: "0px",
                      left: "210px",
                      height: "140px",
                      flexShrink: "0",
                      zIndex: "6",
                      color: "#f79b2d",
                      overflow: "hidden",
                  }}
              >
                  <div
                      style={{
                          position: "absolute",
                          bottom: "0px",
                          left: "0px",
                          width: "644px",
                          height: "65px",
                          overflow: "hidden",
                          zIndex: "7",
                      }}
                  >
                      <div
                          style={{
                              position: "absolute",
                              top: `${topValue}px`,
                              left: "0px",
                              height: "270px",
                              display: "flex",
                              flexDirection: "column",
                              justifyContent: "space-between",
                              transition: "top 0.5s ease-in-out",
                          }}
                      >
                          <div
                              style={{
                                  width: "255px",
                                  position: "relative",
                                  fontWeight: "800",
                                  textAlign: "left",
                                  display: "inline-block",
                                  height: "68px",
                                  flexShrink: "0",
                              }}
                          >
                              Explorez
                          </div>
                          <div
                              style={{
                                  width: "238px",
                                  position: "relative",
                                  fontWeight: "800",
                                  color: "#257693",
                                  display: "inline-block",
                                  height: "68px",
                                  flexShrink: "0",
                              }}
                          >
                              Trouvez
                          </div>
                          <div
                              style={{
                                  width: "477px",
                                  position: "relative",
                                  fontWeight: "800",
                                  color: "#e1cc13",
                                  display: "inline-block",
                                  height: "68px",
                                  flexShrink: "0",
                              }}
                          >
                              Connectez-vous
                          </div>
                          <div
                              style={{
                                  width: "644px",
                                  position: "relative",
                                  fontWeight: "800",
                                  color: "#a85c2f",
                                  display: "inline-block",
                                  height: "68px",
                                  flexShrink: "0",
                              }}
                          >
                              Trouvez votre Succès !
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  );
};

export default Title;
// <Title />
