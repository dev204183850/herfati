/* eslint-disable react/prop-types */
import { Link } from "react-router-dom";
import BellRinging from "./BellRinging";
import Button from "./button";
import Logo from "./logo";
import Navegator from "./navigator";
import Search from "./searsh";
import LogoutButton from "./LogoutButton";
import { useSelector } from "react-redux";

const Navigation = ({ type }) => {
    let variantContent;
    const isAuthenticated = useSelector((state) => state.auth.isLoggedIn);

    switch (type) {
        case 1:
            variantContent = (
                <div
                    style={{
                        backgroundColor: "#fff",
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "center",
                        justifyContent: "center",
                    }}
                >
                    <Navegator />
                    <div
                        style={{
                            width: "1440px",
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "space-between",
                            padding: "24px 105px",
                            boxSizing: "border-box",
                            zIndex: "9999",
                            color: "#18191c",
                        }}
                    >
                        <div
                            style={{
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "center",
                                justifyContent: "center",
                                gap: "32px",
                                paddingRight: "100px",
                            }}
                        >
                            <Logo />
                            <Search />
                        </div>
                        {(isAuthenticated === false) ? (
                            <div
                                style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    alignItems: "center",
                                    justifyContent: "center",
                                    gap: "8px",
                                    fontSize: "16px",
                                    color: "#0a65cc",
                                }}
                            >
                                <Link
                                    to="/register"
                                    style={{
                                        textDecoration: "none",
                                        color: "#424242",
                                    }}
                                >
                                    <Button
                                        size={"medium"}
                                        type={"secondary"}
                                        icon={"right"}
                                        disabled={false}
                                        text={"Register"}
                                    />
                                </Link>
                                <Link
                                    to="/login"
                                    style={{
                                        textDecoration: "none",
                                        color: "#424242",
                                    }}
                                >
                                    <Button
                                        size={"medium"}
                                        type={"primary"}
                                        icon={"right"}
                                        disabled={false}
                                        text={"Login"}
                                    />
                                </Link>
                            </div>
                        ) : (
                            <div
                                style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    alignItems: "center",
                                    justifyContent: "center",
                                    gap: "8px",
                                    fontSize: "16px",
                                    color: "#0a65cc",
                                }}
                            >
                                <LogoutButton />
                            </div>
                        )}
                    </div>
                </div>
            );
            break;
        case 2:
            variantContent = (
                <div
                    style={{
                        backgroundColor: "#fff",
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "center",
                        justifyContent: "center",
                    }}
                >
                    <Navegator />
                    <div
                        style={{
                            width: "1440px",
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "space-between",
                            padding: "24px 105px",
                            boxSizing: "border-box",
                            zIndex: "0",
                            color: "#18191c",
                        }}
                    >
                        <div
                            style={{
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "center",
                                justifyContent: "center",
                                gap: "32px",
                            }}
                        >
                            <Logo />
                            <Search />
                        </div>
                        {isAuthenticated === false ? (
                            <div
                                style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    alignItems: "center",
                                    justifyContent: "center",
                                    gap: "24px",
                                    fontSize: "16px",
                                    color: "#0a65cc",
                                }}
                            >
                                <img
                                    style={{
                                        width: "24px",
                                        position: "relative",
                                        height: "24px",
                                    }}
                                    alt=""
                                    src="/property-1true.svg"
                                />
                                <div
                                    style={{
                                        borderRadius: "3px",
                                        border: "1px solid #cee0f5",
                                        display: "flex",
                                        flexDirection: "row",
                                        alignItems: "center",
                                        justifyContent: "center",
                                        padding: "12px 24px",
                                    }}
                                >
                                    <div
                                        style={{
                                            position: "relative",
                                            lineHeight: "24px",
                                            textTransform: "capitalize",
                                            fontWeight: "600",
                                        }}
                                    >
                                        post A Jobs
                                    </div>
                                </div>
                                <img
                                    style={{
                                        width: "48px",
                                        position: "relative",
                                        borderRadius: "50%",
                                        height: "48px",
                                        objectFit: "cover",
                                    }}
                                    alt=""
                                    src="/ellipse-18@2x.png"
                                />
                            </div>
                        ) : (
                            <div
                                style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    alignItems: "center",
                                    justifyContent: "center",
                                    gap: "24px",
                                    fontSize: "16px",
                                    color: "#0a65cc",
                                }}
                            >
                                <BellRinging />

                                <LogoutButton />

                                <img
                                    style={{
                                        width: "48px",
                                        position: "relative",
                                        borderRadius: "50%",
                                        height: "48px",
                                        objectFit: "cover",
                                    }}
                                    alt=""
                                    src="/ellipse-18@2x.png"
                                />
                            </div>
                        )}
                    </div>
                </div>
            );
            break;
        case 3:
            variantContent = (
                <div
                    style={{
                        backgroundColor: "#fff",
                        height: "auto",
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "center",
                        justifyContent: "center",
                    }}
                >
                    <Navegator />
                    <div
                        style={{
                            width: "1440px",
                            flex: "1",
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "space-between",
                            padding: "24px 105px",
                            boxSizing: "border-box",
                            zIndex: "0",
                            textAlign: "center",
                            fontSize: "16px",
                            color: "#fff",
                            fontFamily: "Roboto",
                        }}
                    >
                        <div
                            style={{
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "center",
                                justifyContent: "center",
                            }}
                        >
                            <Logo />
                        </div>

                        <div
                            style={{
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "center",
                                justifyContent: "center",
                                gap: "24px",
                            }}
                        >
                            <BellRinging />
                            <img
                                style={{
                                    width: "48px",
                                    position: "relative",
                                    borderRadius: "50%",
                                    height: "48px",
                                    objectFit: "cover",
                                }}
                                alt=""
                                src="/ellipse-18@2x.png"
                            />
                        </div>
                    </div>
                </div>
            );
            break;
        case 4 :
            variantContent = (
                <div
                    style={{
                        backgroundColor: "#f1f2f4",
                        height: "90px",
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "center",
                        justifyContent: "center",
                    }}
                >
                    <div
                        style={{
                            width: "1440px",
                            flex: "1",
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "flex-start",
                            padding: "8px 105px",
                            boxSizing: "border-box",
                            zIndex: "0",
                        }}
                    >
                        <Logo />
                    </div>
                </div>
            );
            break;
        default:
            variantContent = <div>Invalid type provided.</div>;
            break;
    }

    return <>{variantContent}</>;
};

export default Navigation;
