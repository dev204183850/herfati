/* eslint-disable react/prop-types */
import { useState } from "react";

const Button = ({ size, type, icon, disabled, text, onClick }) => {
  const [isHovered, setIsHovered] = useState(false);
  const [isDisabled] = useState(disabled);

  // تحديد الأنماط والألوان بناءً على الـ props الممررة

  let buttonStyle = {
    width: icon==="none"? "100%": "auto",
    maxWidth: "180px",
    height: "48px",
    borderRadius: "5px",
    boxSizing: "border-box",
    textAlign: "left",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    fontSize: size === "medium" ? "16px" : "20px",
    fontFamily: "Inter",
    padding: size === "medium" ? "12px 24px" : "16px 32px",
    backgroundColor:
      type === "primary"
        ? "#0a65cc"
        : type === "secondary"
        ? "#e7f0fa"
        : "#cee0f5",
    border: isHovered ? "0.2px solid #0851a3" : "0px dashed #7b61ff",
    pointerEvents: isDisabled ? "none" : "auto",
    opacity: isDisabled ? "0" : "1",
  };

  // إضافة أيقونة إذا كانت موجودة
  let iconComponent = null;
  if (icon === "right") {
    iconComponent = (
      <img
        style={{
          width: "24px",
          height: "24px",
          overflow: "hidden",
          flexShrink: "0",
          marginLeft: "12px",
        }}
        alt=""
        src="/arrow-forward-ios1.svg"
      />
    );
  } else if (icon === "left") {
    iconComponent = (
      <img
        style={{
          width: "24px",
          height: "24px",
          overflow: "hidden",
          flexShrink: "0",
          marginRight: "12px",
        }}
        alt=""
        src="/arrow-forward-ios1.svg"
      />
    );
  }

  return (
      <div
          style={buttonStyle}
          onMouseEnter={() => setIsHovered(true)}
          onMouseLeave={() => setIsHovered(false)}
          onClick={!disabled ? onClick : undefined}
      >
          {icon === "right" && (
              <p
                  style={{
                      fontSize: "16px",
                      textAlign: "center",
                      display: "inline-block",
                      color: "#264466",
                  }}
              >
                  <b> {text}</b>
              </p>
          )}
          {iconComponent}
          {icon === "left" && (
              <p
                  style={{
                      fontSize: "16px",
                      textAlign: "center",
                      display: "inline-block",
                      color: "#264466",
                  }}
              >
                  <b> {text}</b>
              </p>
          )}
          {icon === "none" && (
              <p
                  style={{
                      fontSize: "16px",
                      textAlign: "center",
                      display: "inline-block",
                      color: "#fff",
                  }}
              >
                  <b> {text}</b>
              </p>
          )}
      </div>
  );
};
/*<Button
      size={"medium"} //
      type={"primary"} //secondary autre
      icon={"right"} //left
      disabled={false}
      text={"fouad abbassi"}
    /> */
export default Button;
