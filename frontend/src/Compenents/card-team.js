import { useCallback } from "react";
import PropTypes from "prop-types";

const CardTeam = ({ className = "" }) => {
  const onProperty1Variant2ContainerClick = useCallback(() => {
    // Please sync "Team Detail" to the project
  }, []);

  return (
    <div
      style={{
        borderRadius: "5px",
        border: "1px dashed #9747ff",
        boxSizing: "border-box",
        maxWidth: "100%",
        height: "560px",
        overflow: "hidden",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "space-between",
        padding: "20px",
        textAlign: "left",
        fontSize: "16px",
        color: "#0a65cc",
        fontFamily: "Inter",
      }}
      className={className}
    >
      <div
        style={{
          width: "559px",
          boxShadow: "1px 3px 12px rgba(0, 0, 0, 0.75)",
          borderRadius: "8px",
          backgroundColor: "#fff",
          border: "1px solid #e4e5e8",
          boxSizing: "border-box",
          height: "250px",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          padding: "24px",
        }}
      >
        <div
          style={{
            alignSelf: "stretch",
            flex: "1",
            overflow: "hidden",
            display: "flex",
            flexDirection: "column",
            alignItems: "flex-start",
            justifyContent: "flex-start",
            gap: "8px",
          }}
        >
          <div
            style={{
              alignSelf: "stretch",
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <div
              style={{
                width: "229px",
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <img
                style={{
                  width: "74px",
                  position: "relative",
                  borderRadius: "10px",
                  height: "74px",
                  overflow: "hidden",
                  flexShrink: "0",
                  objectFit: "cover",
                }}
                alt=""
                src="/ellipse-800@2x.png"
              />
              <img
                style={{
                  width: "74px",
                  position: "relative",
                  borderRadius: "10px",
                  height: "74px",
                  overflow: "hidden",
                  flexShrink: "0",
                  objectFit: "cover",
                }}
                alt=""
                src="/ellipse-801@2x.png"
              />
              <img
                style={{
                  width: "74px",
                  position: "relative",
                  borderRadius: "10px",
                  height: "74px",
                  overflow: "hidden",
                  flexShrink: "0",
                  objectFit: "cover",
                }}
                alt=""
                src="/ellipse-802@2x.png"
              />
              <img
                style={{
                  width: "74px",
                  position: "relative",
                  borderRadius: "10px",
                  height: "74px",
                  overflow: "hidden",
                  flexShrink: "0",
                  objectFit: "cover",
                }}
                alt=""
                src="/ellipse-803@2x.png"
              />
              <img
                style={{
                  width: "74px",
                  position: "relative",
                  borderRadius: "10px",
                  height: "74px",
                  overflow: "hidden",
                  flexShrink: "0",
                  objectFit: "cover",
                }}
                alt=""
                src="/ellipse-800@2x.png"
              />
            </div>
            <div
              style={{
                borderRadius: "3px",
                border: "1px solid #cee0f5",
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "flex-start",
                padding: "12px 24px",
              }}
            >
              <div
                style={{
                  position: "relative",
                  lineHeight: "24px",
                  textTransform: "capitalize",
                  fontWeight: "600",
                }}
              >
                show details
              </div>
            </div>
          </div>
          <div
            style={{
              alignSelf: "stretch",
              height: "60px",
              display: "flex",
              flexDirection: "column",
              alignItems: "flex-start",
              justifyContent: "flex-start",
              gap: "4px",
              fontSize: "18px",
              color: "#18191c",
            }}
          >
            <div
              style={{
                alignSelf: "stretch",
                height: "29px",
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <div
                style={{
                  flex: "1",
                  position: "relative",
                  lineHeight: "28px",
                  fontWeight: "500",
                }}
              >
                Groupe Name
              </div>
              <img
                style={{
                  width: "24px",
                  position: "relative",
                  height: "24px",
                  overflow: "hidden",
                  flexShrink: "0",
                }}
                alt=""
                src="/bookmark-border5.svg"
              />
            </div>
            <div
              style={{
                alignSelf: "stretch",
                display: "flex",
                flexDirection: "row",
                alignItems: "flex-start",
                justifyContent: "flex-start",
                gap: "8px",
                fontSize: "12px",
                color: "#020202",
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "flex-start",
                }}
              >
                <div
                  style={{
                    borderRadius: "3px",
                    backgroundColor: "rgba(2, 117, 177, 0.25)",
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "flex-start",
                    justifyContent: "flex-start",
                    padding: "4px 8px",
                  }}
                >
                  <div
                    style={{
                      position: "relative",
                      lineHeight: "12px",
                      textTransform: "uppercase",
                      fontWeight: "600",
                    }}
                  >
                    Full-Time
                  </div>
                </div>
              </div>
              <div
                style={{
                  alignSelf: "stretch",
                  flex: "1",
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "flex-start",
                  gap: "4px",
                  fontSize: "14px",
                  color: "#767f8c",
                }}
              >
                <img
                  style={{
                    width: "18px",
                    position: "relative",
                    height: "18px",
                    overflow: "hidden",
                    flexShrink: "0",
                  }}
                  alt=""
                  src="/location-on4.svg"
                />
                <div
                  style={{
                    flex: "1",
                    position: "relative",
                    lineHeight: "20px",
                  }}
                >
                  settat , el khier
                </div>
              </div>
            </div>
          </div>
          <div
            style={{
              alignSelf: "stretch",
              position: "relative",
              lineHeight: "24px",
              color: "#5e6670",
              display: "inline-block",
              height: "72px",
              flexShrink: "0",
            }}
          >{`description aboit Group smaltravail en void with HTML, JavaScript, CSS, PHP, Symphony and/or Laravel `}</div>
        </div>
      </div>
      <div
        style={{
          width: "561px",
          boxShadow: "1px 3px 12px rgba(0, 0, 0, 0.75)",
          borderRadius: "8px",
          backgroundColor: "#fff",
          border: "2px solid rgba(2, 117, 177, 0.75)",
          boxSizing: "border-box",
          height: "252px",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          padding: "24px",
          cursor: "pointer",
        }}
        onClick={onProperty1Variant2ContainerClick}
      >
        <div
          style={{
            width: "549.1px",
            height: "210.1px",
            overflow: "hidden",
            flexShrink: "0",
            display: "flex",
            flexDirection: "column",
            alignItems: "flex-start",
            justifyContent: "flex-start",
            gap: "8.3px",
          }}
        >
          <div
            style={{
              alignSelf: "stretch",
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <div
              style={{
                width: "238.2px",
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <img
                style={{
                  width: "77px",
                  position: "relative",
                  borderRadius: "10.4px",
                  height: "77px",
                  overflow: "hidden",
                  flexShrink: "0",
                  objectFit: "cover",
                }}
                alt=""
                src="/ellipse-800@2x.png"
              />
              <img
                style={{
                  width: "77px",
                  position: "relative",
                  borderRadius: "10.4px",
                  height: "77px",
                  overflow: "hidden",
                  flexShrink: "0",
                  objectFit: "cover",
                }}
                alt=""
                src="/ellipse-801@2x.png"
              />
              <img
                style={{
                  width: "77px",
                  position: "relative",
                  borderRadius: "10.4px",
                  height: "77px",
                  overflow: "hidden",
                  flexShrink: "0",
                  objectFit: "cover",
                }}
                alt=""
                src="/ellipse-802@2x.png"
              />
              <img
                style={{
                  width: "77px",
                  position: "relative",
                  borderRadius: "10.4px",
                  height: "77px",
                  overflow: "hidden",
                  flexShrink: "0",
                  objectFit: "cover",
                }}
                alt=""
                src="/ellipse-803@2x.png"
              />
              <img
                style={{
                  width: "77px",
                  position: "relative",
                  borderRadius: "10.4px",
                  height: "77px",
                  overflow: "hidden",
                  flexShrink: "0",
                  objectFit: "cover",
                }}
                alt=""
                src="/ellipse-804@2x.png"
              />
            </div>
            <div
              style={{
                borderRadius: "3.12px",
                border: "1px solid #cee0f5",
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "flex-start",
                padding: "12.5px 25px",
              }}
            >
              <div
                style={{
                  position: "relative",
                  lineHeight: "24px",
                  textTransform: "capitalize",
                  fontWeight: "600",
                }}
              >
                show details
              </div>
            </div>
          </div>
          <div
            style={{
              alignSelf: "stretch",
              height: "62.4px",
              display: "flex",
              flexDirection: "column",
              alignItems: "flex-start",
              justifyContent: "flex-start",
              gap: "4.2px",
              fontSize: "18.7px",
              color: "#18191c",
            }}
          >
            <div
              style={{
                alignSelf: "stretch",
                height: "30.2px",
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <div
                style={{
                  flex: "1",
                  position: "relative",
                  lineHeight: "29.12px",
                  fontWeight: "500",
                }}
              >
                Groupe Name
              </div>
              <img
                style={{
                  width: "25px",
                  position: "relative",
                  height: "25px",
                  overflow: "hidden",
                  flexShrink: "0",
                }}
                alt=""
                src="/bookmark-border6.svg"
              />
            </div>
            <div
              style={{
                alignSelf: "stretch",
                display: "flex",
                flexDirection: "row",
                alignItems: "flex-start",
                justifyContent: "flex-start",
                gap: "8.3px",
                fontSize: "12.5px",
                color: "#020202",
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "flex-start",
                }}
              >
                <div
                  style={{
                    borderRadius: "3.12px",
                    backgroundColor: "rgba(2, 117, 177, 0.25)",
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "flex-start",
                    justifyContent: "flex-start",
                    padding: "4.2px 8.3px",
                  }}
                >
                  <div
                    style={{
                      position: "relative",
                      lineHeight: "12.48px",
                      textTransform: "uppercase",
                      fontWeight: "600",
                    }}
                  >
                    Full-Time
                  </div>
                </div>
              </div>
              <div
                style={{
                  alignSelf: "stretch",
                  flex: "1",
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "flex-start",
                  gap: "4.2px",
                  fontSize: "14.6px",
                  color: "#767f8c",
                }}
              >
                <img
                  style={{
                    width: "18.7px",
                    position: "relative",
                    height: "18.7px",
                    overflow: "hidden",
                    flexShrink: "0",
                  }}
                  alt=""
                  src="/location-on5.svg"
                />
                <div
                  style={{
                    flex: "1",
                    position: "relative",
                    lineHeight: "20.8px",
                  }}
                >
                  settat , el khier
                </div>
              </div>
            </div>
          </div>
          <div
            style={{
              alignSelf: "stretch",
              position: "relative",
              fontSize: "16.6px",
              lineHeight: "24.96px",
              color: "#5e6670",
              display: "inline-block",
              height: "74.9px",
              flexShrink: "0",
            }}
          >{`description aboit Group smaltravail en void with HTML, JavaScript, CSS, PHP, Symphony and/or Laravel `}</div>
        </div>
      </div>
    </div>
  );
};

CardTeam.propTypes = {
  className: PropTypes.string,
};

export default CardTeam;
