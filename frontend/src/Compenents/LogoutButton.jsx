import { useDispatch, useSelector } from "react-redux";
import { logoutUser } from "../redux/userAction";
import { Link, useNavigate } from "react-router-dom";
import { useEffect } from "react";
const LogoutButton = () => {
    const navigate = useNavigate();
    // Fonction pour gérer la déconnexion
    const dispatch = useDispatch();
    const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);
    useEffect(() => {
        if (isLoggedIn === false) {
            navigate("/login");
        }
    });
    const handleLogout = async () => {
        try {
            dispatch(logoutUser());
            navigate("/login");
        } catch (error) {
            console.error("Error logging out:", error);
        }
    };

    // Styles inline pour le bouton
    const buttonStyle = {
        backgroundColor: "#f44336", // Couleur de fond
        color: "white", // Couleur du texte
        padding: "10px 20px", // Padding interne
        border: "none", // Pas de bordure
        cursor: "pointer", // Curseur pointer
        fontSize: "16px", // Taille de la police
        margin: "20px", // Marge autour du bouton
        display: "block", // Affichage en bloc
        maxWidth: "180px",
        height: "48px",
        borderRadius: "5px",
        boxSizing: "border-box",
        textAlign: "left",
        textDecoration: "none",
        fontFamily: "Inter",
    };

    return (
        <Link to={"/"}
            style={{
            textDecoration:"none",}}>
            <button style={buttonStyle} onClick={handleLogout}>
                Déconnexion
            </button>
        </Link>
    );
};

export default LogoutButton;
