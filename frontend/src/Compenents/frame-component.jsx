/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React from 'react';
import StepsButton from "./steps-button";

const FrameComponent = ({ handleNext, handlePrevious }) => {
  return (
    <div
      style={{
        width: "681px",
        maxWidth: "100%",
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        textAlign: "left",
        fontSize: "16px",
        color: "#616161",
        fontFamily: "Roboto",
      }}
    >
      <StepsButton type={"previous"} onClick={handlePrevious} />
      <StepsButton type={"next"} onClick={handleNext} />
    </div>
  );
};

export default FrameComponent;
