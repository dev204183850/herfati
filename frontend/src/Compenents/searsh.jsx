import { useState } from "react";
import MenuItem from "./menu-item";

const Search = () => {
    const [isHovered, setIsHovered] = useState(false);
    const [isMenuOpen, setIsMenuOpen] = useState(false);
    const [selectedOption, setSelectedOption] = useState("maroc - settat");
    const [searchInput, setSearchInput] = useState("");

    const handleSelect = (option) => {
        setSelectedOption(option);
    };

    const toggleMenu = () => {
        setIsMenuOpen(!isMenuOpen);
    };

    const handleInputChange = (event) => {
        setSearchInput(event.target.value);
    };

    return (
        <div
            onMouseEnter={() => setIsHovered(true)}
            onMouseLeave={() => setIsHovered(false)}
            style={{
                width: "668px",
                position: "relative",
                borderRadius: "5px",
                backgroundColor: "#fff",
                border: "1px solid #e4e5e8",
                boxSizing: "border-box",
                height: "50px",
                cursor: "pointer",
                zIndex:"9999",
                boxShadow: isHovered
                    ? "0px 0px 10px rgba(0, 0, 0, 0.1)"
                    : "none",
            }}
        >
            <div
                style={{
                    position: "absolute",
                    top: "50%",
                    left: "24px",
                    display: "flex",
                    justifyContent: "flex-start",
                    flexDirection: "row",
                    alignItems: "center",
                    transform: "translateY(-50%)",
                    gap: "20px",
                }}
            >
                <div
                    style={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center",
                        gap: "12px",
                    }}
                    onClick={toggleMenu}
                >
                    <div
                        style={{
                            position: "relative",
                            lineHeight: "20px",
                            fontWeight: "500",
                        }}
                    >
                        {selectedOption}
                    </div>
                    <img
                        style={{
                            width: "16px",
                            height: "16px",
                            overflow: "hidden",
                            flexShrink: "0",
                        }}
                        alt=""
                        src={
                            isMenuOpen
                                ? "/keyboard-arrow-up.svg"
                                : "/keyboard-arrow-down.svg"
                        }
                    />
                </div>
                <div
                    style={{
                        width: "1px",
                        borderRight: "1px solid #e4e5e8",
                        height: "33px",
                    }}
                />
                <div
                    style={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center",
                        gap: "12px",
                        fontSize: "16px",
                        color: "#9199a3",
                    }}
                >
                    <img
                        style={{
                            width: "24px",
                            height: "24px",
                            overflow: "hidden",
                            flexShrink: "0",
                        }}
                        alt=""
                        src="/search.svg"
                    />
                    <input
                        type="text"
                        placeholder="Job title, keyword, company"
                        value={searchInput}
                        onChange={handleInputChange}
                        style={{
                            width: "430px",
                            border: "none",
                            outline: "none",
                            fontSize: "16px",
                            color: "#474c54",
                        }}
                    />
                </div>
            </div>

            {isMenuOpen && (
                <div
                    style={{
                        position: "absolute",
                        top: "100%",
                        left: "0",
                        boxShadow: "0px 16px 40px rgba(24, 25, 28, 0.06)",
                        borderRadius: "5px",
                        backgroundColor: "#fff",
                        border: "1px solid #e4e5e8",
                        boxSizing: "border-box",
                        width: "100%",
                        maxHeight: "300px",
                        overflowY: "auto",
                        zIndex: "auto",
                    }}
                >
                    <div style={{ padding: "10px" }}>
                        {[
                            "Casablanca",
                            "Rabat",
                            "Marrakech",
                            "Fes",
                            "Tangier",
                            "Agadir",
                            "Meknes",
                            "Oujda",
                            "Kenitra",
                            "Tetouan",
                            "Safi",
                            "Nador",
                            "El Jadida",
                            "Beni Mellal",
                            "Mohammedia",
                            "Laayoune",
                            "Dakhla",
                            "Essaouira",
                            "Marrakec",
                            "Fess",
                            "Tangie",
                            "Agadi",
                            "Mekne",
                            "Oujd",
                            "Kenitr",
                            "Tetoua",
                            "Saf",
                        ].map((option, index) => (
                            <MenuItem
                                key={index}
                                text={option}
                                onSelect={handleSelect}
                                checked={selectedOption === option}
                            />
                        ))}
                    </div>
                </div>
            )}
        </div>
    );
};

export default Search;
