import Logo from "./logo.jsx";
import FooterLink from "./footer-link.jsx";
const Footer = () => {
  return (
      <div
          style={{
              width: "100%",
              backgroundColor: "#18191c",
              maxWidth: "100%",
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "flex-start",
              textAlign: "left",
              fontSize: "18px",
              color: "#5e6670",
              fontFamily: "Inter",
              zIndex: "0",
          }}
      >
          <div
              style={{
                  alignSelf: "stretch",
                  height: "334px",
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                  padding: "5px 144px",
                  boxSizing: "border-box",
              }}
          >
              <div
                  style={{
                      width: "268px",
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "flex-start",
                      justifyContent: "flex-start",
                      gap: "24px",
                  }}
              >
                  <Logo />
                  <div
                      style={{
                          width: "268px",
                          height: "95px",
                          display: "flex",
                          flexDirection: "column",
                          alignItems: "flex-start",
                          justifyContent: "flex-start",
                          gap: "12px",
                      }}
                  >
                      <div
                          style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "flex-start",
                              justifyContent: "flex-start",
                          }}
                      >
                          <div
                              style={{
                                  position: "relative",
                                  lineHeight: "28px",
                              }}
                          >
                              Call now:
                          </div>
                          <div
                              style={{
                                  position: "relative",
                                  lineHeight: "28px",
                                  fontWeight: "500",
                                  color: "#fff",
                              }}
                          >
                              +112 634 798 215
                          </div>
                      </div>
                      <div
                          style={{
                              width: "268px",
                              position: "relative",
                              fontSize: "14px",
                              lineHeight: "20px",
                              color: "#767f8c",
                              display: "inline-block",
                          }}
                      >
                          Elgin St. Dallas, Delaware 26000, Settat Sity, Maroc
                      </div>
                  </div>
              </div>
              <div
                  style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "flex-start",
                      justifyContent: "center",
                      gap: "8px",
                      fontSize: "20px",
                      color: "#fff",
                      zIndex:"1"
                  }}
              >
                  <div
                      style={{
                          display: "flex",
                          flexDirection: "column",
                          alignItems: "flex-start",
                          justifyContent: "flex-start",
                          gap: "16px",
                          zIndex: "1",
                      }}
                  >
                      <div
                          style={{
                              width: "200px",
                              position: "relative",
                              lineHeight: "32px",
                              fontWeight: "500",
                              display: "inline-block",
                              zIndex: "1",
                          }}
                      >
                          Quick Link
                      </div>
                      <div
                          style={{
                              display: "flex",
                              flexDirection: "column",
                              alignItems: "flex-start",
                              justifyContent: "flex-start",
                              gap: "4px",
                              textAlign: "center",
                              fontSize: "16px",
                              color: "#9199a3",
                              zIndex: "1",
                          }}
                      >
                          <FooterLink text={"About"} />
                          <FooterLink text={"Pricing"} />
                          <FooterLink text={"Contact"} />
                          <FooterLink text={"Blog"} />
                      </div>
                  </div>
                  <div
                      style={{
                          display: "flex",
                          flexDirection: "column",
                          alignItems: "flex-start",
                          justifyContent: "flex-start",
                          gap: "16px",
                          zIndex: "1",
                      }}
                  >
                      <div
                          style={{
                              width: "200px",
                              position: "relative",
                              lineHeight: "32px",
                              fontWeight: "500",
                              display: "inline-block",
                              zIndex: "1",
                          }}
                      >
                          Candidate
                      </div>
                      <div
                          style={{
                              display: "flex",
                              flexDirection: "column",
                              alignItems: "flex-start",
                              justifyContent: "flex-start",
                              gap: "4px",
                              textAlign: "center",
                              fontSize: "16px",
                              color: "#9199a3",
                              zIndex: "1",
                          }}
                      >
                          <FooterLink text={"Create Team"} />
                          <FooterLink text={"Browser"} />
                          <FooterLink text={"Dashboard"} />
                          <FooterLink text={"Saved Jobs"} />
                      </div>
                  </div>
                  <div
                      style={{
                          display: "flex",
                          flexDirection: "column",
                          alignItems: "flex-start",
                          justifyContent: "flex-start",
                          gap: "16px",
                          zIndex: "1",
                      }}
                  >
                      <div
                          style={{
                              width: "200px",
                              position: "relative",
                              lineHeight: "32px",
                              fontWeight: "500",
                              display: "inline-block",
                              zIndex: "1",
                          }}
                      >
                          Support
                      </div>
                      <div
                          style={{
                              display: "flex",
                              flexDirection: "column",
                              alignItems: "flex-start",
                              justifyContent: "flex-start",
                              gap: "4px",
                              textAlign: "center",
                              fontSize: "16px",
                              color: "#9199a3",
                              zIndex: "1",
                          }}
                      >
                          <FooterLink text={"faqs"} />
                          <FooterLink text={"Privacy Policy"} />
                          <FooterLink text={"Terms & Conditions"} />
                      </div>
                  </div>
                  <div
                      style={{
                          display: "flex",
                          flexDirection: "column",
                          alignItems: "flex-start",
                          justifyContent: "flex-start",
                          gap: "16px",
                          zIndex: "1",
                      }}
                  >
                      <div
                          style={{
                              width: "200px",
                              position: "relative",
                              lineHeight: "32px",
                              fontWeight: "500",
                              display: "inline-block",
                              zIndex: "1",
                          }}
                      >
                          Companies
                      </div>
                      <div
                          style={{
                              display: "flex",
                              flexDirection: "column",
                              alignItems: "flex-start",
                              justifyContent: "flex-start",
                              gap: "4px",
                              textAlign: "center",
                              fontSize: "16px",
                              color: "#9199a3",
                              zIndex: "1",
                          }}
                      >
                          <FooterLink text={"Post a Job"} />
                          <FooterLink text={"Browser"} />
                          <FooterLink text={"Dashboard"} />
                          <FooterLink text={"Saved Candidate"} />
                      </div>
                  </div>
              </div>
          </div>
          <div
              style={{
                  width: "1440px",
                  boxShadow: "0px 1px 0px #2f3338 inset",
                  backgroundColor: "#18191c",
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                  padding: "24px 300px",
                  boxSizing: "border-box",
                  fontSize: "14px",
                  color: "#767f8c",
                  zIndex: "0",
              }}
          >
              <div style={{ position: "relative", lineHeight: "20px" }}>
                  @ 2024 Herfati - All rights Rserved
              </div>
              <div
                  style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "flex-start",
                      justifyContent: "flex-start",
                      gap: "20px",
                  }}
              >
                  <img
                      style={{
                          width: "10px",
                          position: "relative",
                          height: "20px",
                      }}
                      alt=""
                      src="/vector3.svg"
                  />
                  <img
                      style={{
                          width: "20px",
                          position: "relative",
                          height: "20px",
                          overflow: "hidden",
                          flexShrink: "0",
                      }}
                      alt=""
                      src="/youtube-1.svg"
                  />
                  <img
                      style={{
                          width: "20px",
                          position: "relative",
                          height: "20px",
                          overflow: "hidden",
                          flexShrink: "0",
                      }}
                      alt=""
                      src="/instagram-1.svg"
                  />
                  <img
                      style={{
                          width: "20px",
                          position: "relative",
                          height: "20px",
                          overflow: "hidden",
                          flexShrink: "0",
                      }}
                      alt=""
                      src="/twitter-1.svg"
                  />
              </div>
          </div>
      </div>
  );
};
//<Footer />
export default Footer;
