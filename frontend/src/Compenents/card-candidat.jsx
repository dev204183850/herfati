/* eslint-disable react/prop-types */

const CardCandidat = ({ userObjet }) => {
    if (!userObjet) {
        return <p>No user details available</p>; // حالة عندما لا يوجد بيانات وظيفة
    }

    const {
        name,profile_image,
        address,
        skills
    } = userObjet;

    return (
        <div
            style={{
                boxShadow: "1px 3px 12px rgba(0, 0, 0, 0.75)",
                borderRadius: "8px",
                backgroundColor: "#fff",
                border: "1px solid #e4e5e8",
                boxSizing: "border-box",
                width: "368px",
                height: "180px",
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "space-between",
                padding: "24px",
            }}
        >
            <div
                style={{
                    alignSelf: "stretch",
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "flex-start",
                    justifyContent: "flex-start",
                    gap: "6px",
                }}
            >
                <div
                    style={{
                        width: "320px",
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "flex-start",
                        justifyContent: "space-between",
                    }}
                >
                    <div
                        style={{
                            width: "278px",
                            position: "relative",
                            lineHeight: "28px",
                            fontWeight: "500",
                            display: "inline-block",
                            flexShrink: "0",
                        }}
                    >
                        {name}
                    </div>
                    <img
                        style={{
                            width: "24px",
                            position: "relative",
                            height: "24px",
                            overflow: "hidden",
                            flexShrink: "0",
                        }}
                        alt=""
                        src="/bookmark-border.svg"
                    />
                </div>
                <div
                    style={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "flex-start",
                        justifyContent: "flex-start",
                        gap: "8px",
                        fontSize: "12px",
                        color: "#020202",
                    }}
                >
                    <div
                        style={{
                            borderRadius: "3px",
                            backgroundColor: "rgba(247, 155, 45, 0.5)",
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "flex-start",
                            justifyContent: "flex-start",
                            padding: "4px 8px",
                        }}
                    >
                        <div
                            style={{
                                position: "relative",
                                lineHeight: "12px",
                                textTransform: "uppercase",
                                fontWeight: "600",
                            }}
                        >
                            Full-Time
                        </div>
                    </div>
                    <div
                        style={{
                            width: "175px",
                            position: "relative",
                            height: "20px",
                            fontSize: "14px",
                            color: "#767f8c",
                        }}
                    >
                        <div
                            style={{
                                position: "absolute",
                                top: "0%",
                                left: "0%",
                                lineHeight: "20px",
                            }}
                        >
                            {skills}
                        </div>
                    </div>
                </div>
            </div>
            <div
                style={{
                    alignSelf: "stretch",
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center",
                    gap: "12px",
                    fontSize: "16px",
                }}
            >
                <div
                    style={{
                        flex: "1",
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "flex-start",
                        justifyContent: "flex-start",
                        gap: "4px",
                    }}
                >
                    <div
                        style={{
                            alignSelf: "stretch",
                            position: "relative",
                            lineHeight: "24px",
                            fontWeight: "500",
                        }}
                    >
                        {name}
                    </div>
                    <div
                        style={{
                            alignSelf: "stretch",
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "flex-start",
                            gap: "4px",
                            fontSize: "14px",
                            color: "#767f8c",
                        }}
                    >
                        <img
                            style={{
                                width: "18px",
                                position: "relative",
                                height: "18px",
                                overflow: "hidden",
                                flexShrink: "0",
                            }}
                            alt=""
                            src="/location-on.svg"
                        />
                        <div
                            style={{
                                flex: "1",
                                position: "relative",
                                lineHeight: "20px",
                            }}
                        >
                            {address}
                        </div>
                    </div>
                </div>
                {profile_image ? (
                    <img
                        style={{
                            width: "56px",
                            borderRadius: "4px",
                            height: "56px",
                            objectFit: "cover",
                        }}
                        src={profile_image}
                        alt={name}
                    />
                ) : (
                    <img
                        style={{
                            width: "56px",
                            borderRadius: "4px",
                            height: "56px",
                            objectFit: "cover",
                        }}
                        src="/placeholder-image.png"
                        alt="Placeholder"
                    />
                )}
            </div>
        </div>
    );
};

export default CardCandidat;
