// eslint-disable-next-line no-unused-vars
import React, { useState } from "react";
import { MessageAlert } from "./message-alert";
import NotificationItem from "./notification-alert";
const BellRinging = () => {
  const [isNotificationsVisible, setIsNotificationsVisible] = useState(false);
  const [isMessagesVisible, setIsMessagesVisible] = useState(false);
  const [unreadNotifications] = useState(true);
  const [unreadMessages] = useState(false);

  const handleNotificationClick = () => {
    setIsNotificationsVisible(!isNotificationsVisible);
    setIsMessagesVisible(false); // لإخفاء قائمة الرسائل عند عرض قائمة التنبيهات
  };

  const handleMessagesClick = () => {
    setIsMessagesVisible(!isMessagesVisible);
    setIsNotificationsVisible(false); // لإخفاء قائمة التنبيهات عند عرض قائمة الرسائل
  };

 const MessagesDropdown = () => (
     <div
         style={{
             position: "absolute",
             top: "44px",
             left: "-320px",
             width: "424px",
             height: "516px",
             backgroundImage: "url(/Screenshot.png)",
             borderRadius: "4px",
             overflowY: "auto",
             zIndex: "99999",
         }}
     >
         <div
             style={{
                 position: "relative",
                 padding: "16px 24px",
                 fontSize: "18px",
                 fontWeight: "500",
                 borderBottom: "1px solid #e4e5e8",
                 display: "flex",
                 justifyContent: "space-between",
                 alignItems: "center",
                 zIndex: "99999",
             }}
         >
             <span>Messages</span>
             <span style={{ fontSize: "14px", color: "#5e6670" }}>
                 Mark all as read
             </span>
         </div>
         <MessageAlert
             imageSrc="/ellipse-21@2x.png"
             message="Twitter shortlisted you in their job UI/UX Designer"
             time="57 mins ago"
         />
     </div>
 );

  const NotificationsDropdown = () => (
      <div
          style={{
              position: "absolute",
              top: "44px",
              left: "-369px",
              width: "424px",
              height: "516px",
              backgroundColor: "#fff",
              borderRadius: "4px",
              overflowY: "auto",
              zIndex: "99999",
          }}
      >
          <div
              style={{
                  position: "relative",
                  padding: "16px 24px",
                  fontSize: "18px",
                  fontWeight: "500",
                  borderBottom: "1px solid #e4e5e8",
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  zIndex: "99999",
              }}
          >
              <span>Notifications</span>
              <span style={{ fontSize: "14px", color: "#5e6670" }}>
                  Mark all as read
              </span>
          </div>
          <NotificationItem
              text="David applied for your Technical Support position"
              time="12 mins ago"
              onAccept={() => alert("Accepted")}
              onReject={() => alert("Rejected")}
          />
      </div>
  );

  // أيقونة الجرس عند عدم وجود تنبيهات غير مقروءة (variant 1)
  const BellIconDefault = () => (
    <img
      style={{
        width: "24px",
        position: "relative",
        height: "24px",
        cursor: "pointer",
      }}
      alt=""
      src="/property-1false.svg"
    />
  );

  // أيقونة الجرس عند وجود تنبيهات غير مقروءة (variant 2)
  const BellIconUnread = () => (
    <img
      style={{
        width: "24px",
        position: "relative",
        height: "24px",
        cursor: "pointer",
      }}
      alt=""
      src="/property-1true.svg"
    />
  );

  // أيقونة الرسائل عند عدم وجود رسائل جديدة
  const MessageIconDefault = () => (
    <img
      style={{
        width: "24px",
        position: "relative",
        height: "24px",
        cursor: "pointer",
      }}
      alt=""
      src="/bellringing.svg"
    />
  );

  // أيقونة الرسائل عند وجود رسائل جديدة
  const MessageIconUnread = () => (
    <img
      style={{
        width: "24px",
        position: "relative",
        height: "24px",
        cursor: "pointer",
      }}
      alt=""
      src="/bellringing1.svg"
    />
  );

  return (
      <div
          style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "flex-end",
              width: "100px",
              justifyContent: "space-around",
              position: "relative",
              zIndex: "99999",
          }}
      >
          <div onClick={handleNotificationClick}>
              {unreadNotifications ? <BellIconUnread /> : <BellIconDefault />}
          </div>
          {isNotificationsVisible && <NotificationsDropdown />}

          <div onClick={handleMessagesClick}>
              {unreadMessages ? <MessageIconUnread /> : <MessageIconDefault />}
          </div>
          {isMessagesVisible && <MessagesDropdown />}
      </div>
  );
};
//  <BellRinging />
export default BellRinging;
