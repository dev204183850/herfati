/* eslint-disable react/prop-types */
import { useState } from "react";

const FooterLink = ({ text }) => {
  const [hovered, setHovered] = useState(false);

  const handleMouseEnter = () => {
    setHovered(true);
  };

  const handleMouseLeave = () => {
    setHovered(false);
  };

  const textColor = hovered ? "#fff" : "#9199a3";
  const arrowDisplay = hovered ? "inline" : "none";

  return (
      <div
          style={{
              width: "auto",
              boxSizing: "border-box",
              height: "auto",
              overflow: "hidden",
              textAlign: "center",
              fontSize: "16px",
              color: textColor,
              fontFamily: "Inter",
              zIndex: "0",
          }}
          onMouseEnter={handleMouseEnter}
          onMouseLeave={handleMouseLeave}
      >
          <div
              style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "flex-start",
                  padding: "6px 0px",
                  gap: "4px",
                  color: textColor,
                  zIndex: "1",
              }}
          >
              <img
                  style={{
                      width: "20px",
                      position: "relative",
                      height: "20px",
                      overflow: "hidden",
                      flexShrink: "0",
                      display: arrowDisplay,
                      zIndex: "1",
                  }}
                  alt=""
                  src="/arrow-forward.svg"
              />
              <div
                  style={{
                      position: "relative",
                      lineHeight: "24px",
                      zIndex: "1",
                  }}
              >
                  {text}
              </div>
          </div>
      </div>
  );
};
//    <FooterLink text={"footer link"}/>
export default FooterLink;
