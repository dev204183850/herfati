/* eslint-disable react/prop-types */
import { useState } from "react";
import Visibility from "./visibility";

const InputComponent = ({
    label = "Name",
    leftIconSrc,
    showLeftIcon = false,
    showRightIcon = false,
    onValueChange, // Function to pass the input value to the parent component
}) => {
    const [isFocused, setIsFocused] = useState(false);
    const [isVisible, setIsVisible] = useState(showRightIcon); // State to track visibility
    const [inputValue, setInputValue] = useState(""); // State to track input value

    const handleVisibilityChange = (visibility) => {
        setIsVisible(visibility);
    };

    const handleInputChange = (e) => {
        const value = e.target.value;
        setInputValue(value);
        onValueChange(value); // Pass the input value to the parent component
    };

    const labelStyle = {
        color: "#111111",
        fontSize: isFocused ? "14px" : "18px",
        position: "absolute",
        pointerEvents: "none",
        left: "5px",
        top: isFocused ? "-6px" : "10px",
        transition: "0.2s ease all",
    };

    const barBeforeAfterStyle = {
        content: '""',
        height: "2px",
        width: "0",
        bottom: "1px",
        position: "absolute",
        background: "#5264AE",
        transition: "0.2s ease all",
    };

    return (
        <div
            style={{
                position: "relative",
                display: "flex",
                alignItems: "center",
                width: "439px",
            }}
        >
            {showLeftIcon && (
                <img
                    src={leftIconSrc}
                    alt="left icon"
                    style={{
                        width: "24px",
                        height: "24px",
                        marginRight: "10px",
                    }}
                />
            )}
            <div style={{ position: "relative" }}>
                <input
                    required
                    type={!isVisible ? "text" : "password"} // Change type based on visibility
                    value={inputValue} // Bind the input value to the state
                    onChange={handleInputChange} // Handle input change
                    style={{
                        fontSize: "16px",
                        padding: "0px 10px 10px 5px",
                        margin:"20px 0px",
                        display: "block",
                        width: "371px",
                        border: "none",
                        borderBottom: "1px solid #515151",
                        background: "transparent",
                        outline: "none",
                    }}
                    onFocus={() => setIsFocused(true)}
                    onBlur={(e) => setIsFocused(e.target.value !== "")}
                />
                <span
                    style={{
                        position: "absolute",
                        height: "60%",
                        width: "371px",
                        top: "25%",
                        left: "0",
                        pointerEvents: "none",
                        opacity: "0",
                        background: "#5264AE",
                        transition: "opacity 0.3s ease",
                    }}
                ></span>
                <span style={{ display: "block", width: "371px" }}>
                    <span
                        style={{ ...barBeforeAfterStyle, left: "50%" }}
                    ></span>
                    <span
                        style={{ ...barBeforeAfterStyle, right: "50%" }}
                    ></span>
                </span>
                <label style={labelStyle}>{label}</label>
            </div>
            {showRightIcon && (
                <Visibility onVisibilityChange={handleVisibilityChange} />
            )}
        </div>
    );
};

export default InputComponent;
