import { Link, useNavigate } from "react-router-dom";
import SignUpEtLogin from "./login-register";
import Title from "./title";
import { useEffect} from "react";
import { useSelector } from "react-redux";

const Home = ({ variante, loginOrRegester }) => {

    const navigate = useNavigate();
    const isAuthenticated = useSelector((state) => state.auth.isLoggedIn);

    useEffect(() => {
        if (variante === "home" && isAuthenticated===true ) {
            navigate("/");
        }
    }, [isAuthenticated, navigate, variante]);
    const variantDeux = () => (
        <div style={{ width: "100%", position: "relative", height: "800px" }}>
            <div
                style={{
                    position: "absolute",
                    width: "100%",
                    top: "0px",
                    right: "0%",
                    left: "0%",
                    backgroundColor: "rgba(241, 242, 244, 0.6)",
                    height: "654px",
                    overflow: "hidden",
                }}
            >
                <div
                    style={{
                        position: "absolute",
                        width: "66.94%",
                        top: "57px",
                        right: "23.06%",
                        left: "10%",
                        height: "400px",
                    }}
                >
                    <div
                        style={{
                            position: "absolute",
                            top: "0px",
                            left: "0px",
                            display: "flex",
                            flexDirection: "column",
                            alignItems: "flex-start",
                            justifyContent: "flex-start",
                            gap: "24px",
                        }}
                    >
                        {/**title */}
                        <Title />
                        {/**description */}
                        <div
                            style={{
                                width: "710px",
                                position: "relative",
                                fontSize: "18px",
                                lineHeight: "28px",
                                color: "#5e6670",
                                display: "inline-block",
                            }}
                        >
                            <p>
                                {" "}
                                une plateforme innovante visant à faciliter la
                                recherche demploi et le recrutement.
                                Rejoignez-nous dès maintenant pour une
                                expérience de recherche demploi et de
                                recrutement unique et innovante
                            </p>
                        </div>
                        {/**regester */}
                        {(isAuthenticated===false)? (
                            <Link
                                to="/register"
                                style={{ textDecoration: "none", color: "#424242" }}
                            >
                                <div
                                    style={{
                                        borderRadius: "15px",
                                        border: "1px solid #0a65cc",
                                        overflow: "hidden",
                                        display: "flex",
                                        flexDirection: "row",
                                        alignItems: "center",
                                        justifyContent: "center",
                                        fontSize: "16px",
                                        color: "#0a65cc",
                                    }}
                                >
                                    <div
                                        style={{
                                            borderRadius: "4px",
                                            backgroundColor: "#e7f0fa",
                                            display: "flex",
                                            flexDirection: "row",
                                            alignItems: "center",
                                            justifyContent: "center",
                                            padding: "16px 32px",
                                        }}
                                    >
                                        <div
                                            style={{
                                                position: "relative",
                                                lineHeight: "24px",
                                                textTransform: "capitalize",
                                                fontWeight: "600",
                                            }}
                                        >
                                            Register now
                                        </div>
                                    </div>
                                </div>
                            </Link>) : (<div></div>)}
                    </div>
                </div>
                {/** 4 smale card */}
                <div
                    style={{
                        position: "absolute",
                        width: "100%",
                        top: "417px",
                        right: "0%",
                        left: "0%",
                        height: "96px",
                        fontSize: "24px",
                        color: "#18191c",
                    }}
                >
                    <div
                        style={{
                            position: "absolute",
                            top: "0px",
                            left: "144px",
                            borderRadius: "8px",
                            backgroundColor: "#fff",
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "center",
                            padding: "12px 5px",
                            gap: "8px",
                        }}
                    >
                        <div
                            style={{
                                borderRadius: "4px",
                                backgroundColor: "#e7f0fa",
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "flex-start",
                                justifyContent: "flex-start",
                                padding: "16px",
                            }}
                        >
                            <img
                                style={{
                                    width: "40px",
                                    position: "relative",
                                    height: "40px",
                                    overflow: "hidden",
                                    flexShrink: "0",
                                }}
                                alt=""
                                src="./buildingsduotone-1 copy.svg"
                            />
                        </div>
                        <div
                            style={{
                                display: "flex",
                                flexDirection: "column",
                                alignItems: "flex-start",
                                justifyContent: "flex-start",
                                gap: "6px",
                            }}
                        >
                            <div
                                style={{
                                    width: "180px",
                                    position: "relative",
                                    lineHeight: "32px",
                                    fontWeight: "500",
                                    display: "inline-block",
                                }}
                            >
                                175,324
                            </div>
                            <div
                                style={{
                                    width: "180px",
                                    position: "relative",
                                    fontSize: "16px",
                                    lineHeight: "24px",
                                    color: "#767f8c",
                                    display: "inline-block",
                                }}
                            >
                                Live Job
                            </div>
                        </div>
                    </div>
                    <div
                        style={{
                            position: "absolute",
                            top: "0px",
                            left: "438px",
                            boxShadow: "0px 12px 48px rgba(0, 44, 109, 0.1)",
                            borderRadius: "8px",
                            backgroundColor: "#fff",
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "center",
                            padding: "12px 5px",
                            gap: "8px",
                        }}
                    >
                        <div
                            style={{
                                borderRadius: "4px",
                                backgroundColor: "#0a65cc",
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "flex-start",
                                justifyContent: "flex-start",
                                padding: "16px",
                            }}
                        >
                            <img
                                style={{
                                    width: "40px",
                                    position: "relative",
                                    height: "40px",
                                    overflow: "hidden",
                                    flexShrink: "0",
                                }}
                                alt=""
                                src="/buildingsduotone-1.svg"
                            />
                        </div>
                        <div
                            style={{
                                display: "flex",
                                flexDirection: "column",
                                alignItems: "flex-start",
                                justifyContent: "flex-start",
                                gap: "6px",
                            }}
                        >
                            <div
                                style={{
                                    width: "180px",
                                    position: "relative",
                                    lineHeight: "32px",
                                    fontWeight: "500",
                                    display: "inline-block",
                                }}
                            >
                                97,354
                            </div>
                            <div
                                style={{
                                    width: "180px",
                                    position: "relative",
                                    fontSize: "16px",
                                    lineHeight: "24px",
                                    color: "#767f8c",
                                    display: "inline-block",
                                }}
                            >
                                Companies
                            </div>
                        </div>
                    </div>
                    <div
                        style={{
                            position: "absolute",
                            top: "0px",
                            left: "732px",
                            borderRadius: "8px",
                            backgroundColor: "#fff",
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "center",
                            padding: "12px 5px",
                            gap: "8px",
                        }}
                    >
                        <div
                            style={{
                                borderRadius: "4px",
                                backgroundColor: "#e7f0fa",
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "flex-start",
                                justifyContent: "flex-start",
                                padding: "16px",
                            }}
                        >
                            <img
                                style={{
                                    width: "40px",
                                    position: "relative",
                                    height: "40px",
                                    overflow: "hidden",
                                    flexShrink: "0",
                                }}
                                alt=""
                                src="/usersduotone-1.svg"
                            />
                        </div>
                        <div
                            style={{
                                display: "flex",
                                flexDirection: "column",
                                alignItems: "flex-start",
                                justifyContent: "flex-start",
                                gap: "6px",
                            }}
                        >
                            <div
                                style={{
                                    width: "180px",
                                    position: "relative",
                                    lineHeight: "32px",
                                    fontWeight: "500",
                                    display: "inline-block",
                                }}
                            >
                                847,154
                            </div>
                            <div
                                style={{
                                    width: "180px",
                                    position: "relative",
                                    fontSize: "16px",
                                    lineHeight: "24px",
                                    color: "#767f8c",
                                    display: "inline-block",
                                }}
                            >
                                Candidates
                            </div>
                        </div>
                    </div>
                    <div
                        style={{
                            position: "absolute",
                            top: "0px",
                            left: "1026px",
                            borderRadius: "8px",
                            backgroundColor: "#fff",
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "center",
                            padding: "12px 5px",
                            gap: "8px",
                        }}
                    >
                        <div
                            style={{
                                borderRadius: "4px",
                                backgroundColor: "#e7f0fa",
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "flex-start",
                                justifyContent: "flex-start",
                                padding: "16px",
                            }}
                        >
                            <img
                                style={{
                                    width: "40px",
                                    position: "relative",
                                    height: "40px",
                                    overflow: "hidden",
                                    flexShrink: "0",
                                }}
                                alt=""
                                src="/briefcaseduotone-1.svg"
                            />
                        </div>
                        <div
                            style={{
                                display: "flex",
                                flexDirection: "column",
                                alignItems: "flex-start",
                                justifyContent: "flex-start",
                                gap: "6px",
                            }}
                        >
                            <div
                                style={{
                                    width: "180px",
                                    position: "relative",
                                    lineHeight: "32px",
                                    fontWeight: "500",
                                    display: "inline-block",
                                }}
                            >
                                7,532
                            </div>
                            <div
                                style={{
                                    width: "180px",
                                    position: "relative",
                                    fontSize: "16px",
                                    lineHeight: "24px",
                                    color: "#767f8c",
                                    display: "inline-block",
                                }}
                            >
                                New Jobs
                            </div>
                        </div>
                    </div>
                </div>
                {/**image persoone */}
                <img
                    style={{
                        position: "absolute",
                        top: "4px",
                        right: "144px",
                        width: "265px",
                        height: "507px",
                        objectFit: "contain",
                    }}
                    alt=""
                    src="/pngpersonne@2x copy.png"
                />
            </div>
        </div>
    );
    const variantThree = () => (
        <div
            style={{
                width: "100%",
                position: "relative",
                height: "800px",
                overflow: "hidden",
                flexShrink: "0",
                fontSize: "32px",
                fontFamily: "Poppins",
            }}
        >
            <div
                style={{
                    position: "absolute",
                    width: "100%",
                    top: "0px",
                    right: "0%",
                    left: "0%",
                    backgroundColor: "#f6fbfc",
                    height: "654px",
                }}
            >
                <img
                    style={{
                        position: "absolute",
                        top: "0px",
                        right: "0px",
                        width: "869px",
                        height: "654px",
                        objectFit: "contain",
                    }}
                    alt=""
                    src="/pose-0009@2x.png"
                />
                <div
                    style={{
                        position: "absolute",
                        top: "0px",
                        left: "-326px",
                        borderRadius: "50%",
                        backgroundColor: "rgba(39, 119, 148, 0.75)",
                        width: "654px",
                        height: "654px",
                    }}
                />
                <div
                    style={{
                        position: "absolute",
                        top: "calc(50% - 300px)",
                        left: "calc(50% - 576px)",
                        borderRadius: "25px",
                        width: "500px",
                        height: "600px",
                        overflow: "hidden",
                        backgroundImage: "url('/sing-up-et-login@3x.png')",
                        backgroundSize: "cover",
                        backgroundRepeat: "no-repeat",
                        backgroundPosition: "top",
                    }}
                >
                    <SignUpEtLogin variant={loginOrRegester} />
                    <img
                        style={{
                            position: "absolute",
                            top: "17px",
                            right: "16px",
                            width: "40px",
                            height: "40px",
                        }}
                        alt=""
                        src="/group-210.svg"
                    />
                </div>
            </div>
        </div>
    );

    return variante === "home" ? variantDeux() : (variante === "login" &&
              !isAuthenticated) ||
          (variante === "register" &&
              !isAuthenticated)
        ? variantThree()
            :  variante="home";
};


export default Home;
