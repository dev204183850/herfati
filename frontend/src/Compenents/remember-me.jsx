/* eslint-disable react/prop-types */
import { useState } from "react";

const RememberMe = ({text}) => {
  const [isChecked, setIsChecked] = useState(false);

  const handleCheckboxClick = () => {
    setIsChecked(!isChecked);
  };

  return (
    <div
      style={{
        height: "24px",
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        gap: "4px",
        cursor: "pointer",
      }}
      onClick={handleCheckboxClick}
    >
      <img
        style={{
          width: "24px",
          position: "relative",
          height: "24px",
          overflow: "hidden",
          flexShrink: "0",
        }}
        alt=""
        src={isChecked ? "/check-box.svg" : "/check-box-outline-blank.svg"}
      />
      <div style={{ position: "relative", fontWeight: "300" }}> {text} </div>
    </div>
  );
};

export default RememberMe;
