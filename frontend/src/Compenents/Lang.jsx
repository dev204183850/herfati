import { useState } from "react";
import MenuItem from "./menu-item";

const Lang = () => {
    const [isHovered, setIsHovered] = useState(false);
    const [isMenuOpen, setIsMenuOpen] = useState(false);
    const [selectedOption, setSelectedOption] = useState("English");

    const handleSelect = (option) => {
        setSelectedOption(option);
        setIsMenuOpen(false);
    };

    const toggleMenu = () => {
        setIsMenuOpen(!isMenuOpen);
    };

    return (
        <div
            onMouseEnter={() => setIsHovered(true)}
            onMouseLeave={() => setIsHovered(false)}
            onClick={toggleMenu}
            style={{
                position: "relative",
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                padding: "8px 0px",
                backgroundColor: "#f1f2f4",
                gap: "8px",
                width: "fit-content",
                marginLeft:"150px", //
                cursor: "pointer",
                color: isHovered ? "#037dc2" : "rgba(66, 66, 66, 0.75)",
                boxShadow: isHovered
                    ? "0px 0px 0px rgba(0, 0, 0, 0.1)"
                    : "none",
            }}
        >
            <img
                style={{
                    width: "24px",
                    height: "24px",
                    overflow: "hidden",
                    flexShrink: "0",
                }}
                alt=""
                src="/language.svg"
            />
            <div
                style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                    position: "relative",
                }}
            >
                <div
                    style={{
                        letterSpacing: "0.5px",
                        lineHeight: "24px",
                        fontWeight: "500",
                    }}
                >
                    {selectedOption}
                </div>
                <img
                    style={{
                        width: "24px",
                        height: "24px",
                        overflow: "hidden",
                        flexShrink: "0",
                    }}
                    alt=""
                    src={
                        isMenuOpen
                            ? "/keyboard-arrow-up.svg"
                            : "/expand-more.svg"
                    }
                />
            </div>

            {isMenuOpen && (
                <div
                    style={{
                        position: "absolute",
                        top: "130%",
                        right: "0",
                        boxShadow: "0px 16px 40px rgba(24, 25, 28, 0.06)",
                        borderRadius: "5px",
                        backgroundColor: "#fff",
                        border: "1px solid #e4e5e8",
                        display: "flex",
                        flexDirection: "column",
                        padding: "8px",
                        color: "#474c54",
                        zIndex: "99999",
                    }}
                >
                    {[
                        "Arabic",
                        "Francais",
                        "English",
                        "Spanish",
                        "Tamazigh",
                    ].map((option, index) => (
                        <MenuItem
                            key={index}
                            text={option}
                            onSelect={handleSelect}
                            checked={selectedOption === option}
                        />
                    ))}
                </div>
            )}
        </div>
    );
};

export default Lang;
