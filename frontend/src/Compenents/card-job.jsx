/* eslint-disable react/prop-types */
import { useState } from "react";

const CardJob = ({ jobObjet }) => {
    const [isHovered, setIsHovered] = useState(false);

    if (!jobObjet) {
        return <p>No job details available</p>;
    }

    const {
        job_image,
        title,
        salary,
        location,
        user_id = { name: "Not specified" },
    } = jobObjet;

    const handleMouseEnter = () => setIsHovered(true);
    const handleMouseLeave = () => setIsHovered(false);

    return (
        <div
            style={{
                position: "relative",
                left: "20px",
                boxShadow: isHovered
                    ? "1px 5px 15px rgba(0, 0, 0, 0.9)"
                    : "1px 3px 12px rgba(0, 0, 0, 0.75)",
                borderRadius: "8px",
                backgroundColor: "#fff",
                border: isHovered
                    ? "2px solid rgba(247, 155, 45, 0.75)"
                    : "1px solid #e4e5e8",
                boxSizing: "border-box",
                width: "368px",
                height: "180px",
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "space-between",
                padding: "24px",
                cursor: "pointer",
                transition: "all 0.6s",
                fontSize: isHovered ? "19.1px" : "16px",
            }}
            onMouseEnter={handleMouseEnter}
            onMouseLeave={handleMouseLeave}
        >
            <div
                style={{
                    width: isHovered ? "339.6px" : "320px",
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "flex-start",
                    justifyContent: "flex-start",
                    gap: "6px",
                    transition: "all 0.6s",
                }}
            >
                <div
                    style={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "flex-start",
                        justifyContent: "space-between",
                        width: "100%",
                    }}
                >
                    <div
                        style={{
                            width: isHovered ? "295px" : "278px",
                            lineHeight: isHovered ? "29.71px" : "28px",
                            fontWeight: "500",
                            flexShrink: "0",
                            transition: "all 0s",
                        }}
                    >
                        {title}
                    </div>
                    <img
                        style={{
                            width: isHovered ? "25.5px" : "24px",
                            height: isHovered ? "25.5px" : "24px",
                            overflow: "hidden",
                            flexShrink: "0",
                        }}
                        alt="bookmark"
                        src={
                            isHovered
                                ? "/bookmark-border1.svg"
                                : "/bookmark-border.svg"
                        }
                    />
                </div>
                <div
                    style={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "flex-start",
                        justifyContent: "flex-start",
                        gap: isHovered ? "8.5px" : "8px",
                        fontSize: isHovered ? "12.7px" : "12px",
                        color: "#020202",
                    }}
                >
                    <div
                        style={{
                            borderRadius: isHovered ? "3.18px" : "3px",
                            backgroundColor: "rgba(247, 155, 45, 0.5)",
                            display: "flex",
                            alignItems: "center",
                            padding: isHovered ? "4.2px 8.5px" : "4px 8px",
                        }}
                    >
                        <div
                            style={{
                                textTransform: "uppercase",
                                fontWeight: "600",
                                lineHeight: isHovered ? "12.73px" : "12px",
                            }}
                        >
                            Full-Time
                        </div>
                    </div>
                    <div
                        style={{
                            width: isHovered ? "185.7px" : "175px",
                            height: isHovered ? "21.2px" : "20px",
                            fontSize: isHovered ? "14.9px" : "14px",
                            color: "#767f8c",
                        }}
                    >
                        <div
                            style={{
                                lineHeight: isHovered ? "21.22px" : "20px",
                            }}
                        >
                            Salary: ${salary}
                        </div>
                    </div>
                </div>
            </div>
            <div
                style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center",
                    gap: isHovered ? "12.7px" : "12px",
                    fontSize: isHovered ? "17px" : "16px",
                    width: isHovered ? "339.6px" : "100%",
                    transition: "all 0.6s",
                }}
            >
                <div
                    style={{
                        flex: "1",
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "flex-start",
                        gap: isHovered ? "4.2px" : "4px",
                    }}
                >
                    <div
                        style={{
                            fontWeight: "500",
                            lineHeight: isHovered ? "25.47px" : "24px",
                        }}
                    >
                        {user_id.name}
                    </div>
                    <div
                        style={{
                            display: "flex",
                            alignItems: "center",
                            gap: isHovered ? "4.2px" : "4px",
                            fontSize: isHovered ? "14.9px" : "14px",
                            color: "#767f8c",
                        }}
                    >
                        <img
                            style={{
                                width: isHovered ? "19.1px" : "18px",
                                height: isHovered ? "19.1px" : "18px",
                                overflow: "hidden",
                                flexShrink: "0",
                            }}
                            alt="location"
                            src={
                                isHovered
                                    ? "/location-on1.svg"
                                    : "/location-on.svg"
                            }
                        />
                        <div
                            style={{
                                lineHeight: isHovered ? "21.22px" : "20px",
                            }}
                        >
                            {location}
                        </div>
                    </div>
                </div>
                <img
                    style={{
                        width: isHovered ? "59.4px" : "56px",
                        height: isHovered ? "59.4px" : "56px",
                        borderRadius: "4px",
                        objectFit: "cover",
                    }}
                    src={job_image || "/placeholder-image.png"}
                    alt={title}
                />
            </div>
        </div>
    );
};

export default CardJob;
