/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React, { useState } from "react";

const StepsButton = ({ type, onClick, disabled }) => {
  const [isHovered, setIsHovered] = useState(false);

  const handleMouseEnter = () => {
    if (!disabled) {
      setIsHovered(true);
    }
  };

  const handleMouseLeave = () => {
    if (!disabled) {
      setIsHovered(false);
    }
  };

  const containerStyle = {
    display: "flex",
    alignItems: "center",
    justifyContent: type === "previous" ? "flex-start" : "flex-end",
    color: disabled ? "#616161" : isHovered ? "#e91e63" : "#616161",
    opacity: disabled ? 0.5 : 1,
    cursor: disabled ? "default" : "pointer",
    width: "fit-content",
  };

  const arrowSrc =
    type === "previous"
      ? isHovered
        ? "/keyboard-arrow-left1.svg"
        : "/keyboard-arrow-left.svg"
      : isHovered
      ? "/keyboard-arrow-right1.svg"
      : "/keyboard-arrow-right.svg";

  return (
    <div
      style={containerStyle}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
      onClick={!disabled ? onClick : undefined}
    >
      {type === "previous" && (
        <img style={{ width: "24px", height: "24px" }} alt="" src={arrowSrc} />
      )}
      <div style={{ margin: "0 8px" }}>
        {type === "previous" ? "Previous" : "Next"}
      </div>
      {type === "next" && (
        <img style={{ width: "24px", height: "24px" }} alt="" src={arrowSrc} />
      )}
    </div>
  );
};
/*  <StepsButton
    type={"previous"}//next
    disabled={false} /> */
export default StepsButton;
