import { Link } from "react-router-dom";
import Lang from "./Lang";
import LinkItem from "./link-item";

const Navegator = () => {
    return (
        <div
            style={{
                width: "100%",
                backgroundColor: "#F1F2F4",
                maxWidth: "100%",
                height: "48px",
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
                boxSizing: "border-box",
                textAlign: "left",
                fontSize: "14px",
                color: "#424242",
                fontFamily: "Inter",
                padding:"0px 144px",
            }}
        >
            <div
                style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center",
                    gap: "32px",
                }}
            >
                <Link
                    to="/"
                    style={{ textDecoration: "none", color: "#424242" }}
                >
                    <LinkItem text={"Home"} />
                </Link>
                <Link
                    to="/freejobs"
                    style={{ textDecoration: "none", color: "#424242" }}
                >
                    <LinkItem text={"Find Job"} />
                </Link>
                <Link
                    to="/candidats"
                    style={{ textDecoration: "none", color: "#424242" }}
                >
                    <LinkItem text={"Find Candidats"} />
                </Link>
                <Link
                    to="/companys"
                    style={{ textDecoration: "none", color: "#424242" }}
                >
                    <LinkItem text={"Find Companie"} />
                </Link>
                <Link
                    to="/teams"
                    style={{ textDecoration: "none", color: "#424242" }}
                >
                    <LinkItem text={"Find Team"} />
                </Link>
                <Link
                    to="/dashboard"
                    style={{ textDecoration: "none", color: "#424242" }}
                >
                    <LinkItem text={"Dashboard"} />
                </Link>
                <Link
                    to="/"
                    style={{ textDecoration: "none", color: "#424242" }}
                >
                    <LinkItem text={"Customer Supports"} />
                </Link>
            </div>
            <Lang />
        </div>
    );
};

export default Navegator;
