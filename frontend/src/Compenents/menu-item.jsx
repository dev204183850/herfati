/* eslint-disable react/prop-types */
import { useState } from "react";

const MenuItem = ({ text, onSelect, checked, setChecked }) => {
    const [isHovered, setIsHovered] = useState(false);

    const handleMouseEnter = () => {
        setIsHovered(true);
    };

    const handleMouseLeave = () => {
        setIsHovered(false);
    };

    const handleClick = () => {
        onSelect(text);
        setChecked(text);
    };

    const getStyle = () => {
        if (checked) {
            // النمط المحدد (المتغير 3)
            return {
                backgroundColor: "#d0d0d0",
                fontWeight: "500",
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
            };
        } else if (isHovered) {
            // النمط عند التحويم (المتغير 2)
            return {
                backgroundColor: "#f1f2f4",
                display: "flex",
                alignItems: "center",
                justifyContent: "flex-start",
            };
        } else {
            // النمط الافتراضي (المتغير 1)
            return {
                display: "flex",
                alignItems: "center",
                justifyContent: "flex-start",
            };
        }
    };

    return (
        <div
            onMouseEnter={handleMouseEnter}
            onMouseLeave={handleMouseLeave}
            onClick={handleClick}
            style={{
                width: "200px",
                borderRadius: "4px",
                padding: "8px 46px",
                boxSizing: "border-box",
                cursor: "pointer",
                float:"left",
                ...getStyle(),
            }}
        >
            <div
                style={
                    checked
                        ? {
                              color: "#18191C",
                              fontWeight: "bold",
                          }
                        : {}
                }
            >
                {text}
            </div>
            {checked && (
                <img
                    style={{ width: "16px", height: "16px" }}
                    alt="Selected"
                    src="/check.svg"
                />
            )}
        </div>
    );
};

export default MenuItem;
  /*  const [selectedOption, setSelectedOption] = useState("1");

    const handleSelect = (option) => {
        setSelectedOption(option);
    };
*/
/*
return (
        <div style={{ width: "250px", margin: "20px" }}>
            {["option 1", "option 2", "option 3"].map((option, index) => (
                <MenuItem
                    key={index}
                    text={option}
                    onSelect={handleSelect}
                    checked={selectedOption === option}
                    setChecked={setSelectedOption}
                />
            ))}
        </div>
    );
*/
