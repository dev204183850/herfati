/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

const TeamShow = () => {
        const navigate = useNavigate();

        useEffect(() => {
            if (!window.localStorage.getItem("Acces_Token")) {
                navigate("/login");
            }
        }, []);
    return <>page team show</>;
};

export default TeamShow;
