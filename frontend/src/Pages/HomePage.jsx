/* eslint-disable react/prop-types */

import Home from "../Compenents/home";

const HomePage = ({ type }) => {

    return type === "login" ? (
        <Home variante={"login"} />
    ) : type === "register" ? (
            <Home variante={"register"} />
    ) : (
        <>
            <Home variante={"home"} />
        </>
    );
};

export default HomePage;
