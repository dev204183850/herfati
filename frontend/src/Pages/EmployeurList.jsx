// src/pages/JobListPage.jsx
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getUsers } from "../redux/userAction";
import CardCandidat from "../Compenents/card-candidat";

const EmployeurList = () => {
    const dispatch = useDispatch();
    const users = useSelector((state) => state.auth.users);
    const [page, setPage] = useState(1);
    const [arrNembre, setArrNumbers] = useState([]);
    useEffect(() => {
        if (page > 2) {
            setArrNumbers([page - 2, page - 1, page, page + 1, page + 2]);
        } else {
            setArrNumbers([1, 2, 3, 4, 5]);
        }
    }, [page]);
    useEffect(() => {
        dispatch(getUsers(page));
    }, [page, dispatch]);

    const handleNextPage = () => {
        setPage(page + 1);
    };

    const handlePrevPage = () => {
        if (page > 1) {
            setPage(page - 1);
        }
    };

    return (
        <div
            style={{
                width: "100%",
                backgroundColor: "#fff",
                overflow: "hidden",
                textAlign: "left",
                fontSize: "18px",
                color: "#5e6670",
                fontFamily: "Inter, sans-serif",
            }}
        >
            <div
                style={{
                    width: "100%",
                    backgroundColor: "rgba(3, 125, 194, 0.1)",
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                    padding: "24px 144px",
                    boxSizing: "border-box",
                    textAlign: "center",
                    color: "#18191c",
                }}
            >
                <div style={{ lineHeight: "28px", fontWeight: "500" }}>
                    Job List
                </div>
                <div
                    style={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "flex-start",
                        justifyContent: "flex-start",
                        gap: "8px",
                        textAlign: "left",
                        fontSize: "14px",
                        color: "#767f8c",
                    }}
                >
                    <div style={{ lineHeight: "20px" }}>Home</div>
                    <div style={{ lineHeight: "20px" }}>/</div>
                    <div
                        style={{
                            lineHeight: "20px",
                            color: "#18191c",
                        }}
                    >
                        Find Job
                    </div>
                </div>
            </div>

            <div
                style={{
                    margin: "48px 12%",
                    height: "1004px",
                    display: "flex",
                    flexDirection: "row",
                    flexWrap: "wrap",
                    alignItems: "center",
                    alignContent: "space-between",
                    justifyContent: "space-between",
                    padding: "4px",
                    boxSizing: "border-box",
                    color: "#18191c",
                }}
            >
                {users.map((userObjet) => (
                    <CardCandidat key={userObjet.id} userObjet={userObjet} />
                ))}
            </div>

            <div
                style={{
                    width: "auto",
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center",
                    gap: "16px",
                    textAlign: "center",
                    fontSize: "14px",
                    marginBottom: "48px",
                }}
            >
                <div
                    onClick={handlePrevPage}
                    style={{
                        borderRadius: "84px",
                        backgroundColor: "#ecf9ea",
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "flex-start",
                        justifyContent: "flex-start",
                        padding: "12px",
                        cursor: "pointer",
                    }}
                >
                    <img
                        style={{
                            width: "24px",
                            height: "24px",
                            overflow: "hidden",
                            flexShrink: "0",
                        }}
                        alt="Previous"
                        src="/arrow-back.svg"
                    />
                </div>

                {arrNembre.map((nember, index) => (
                    <div
                        key={index}
                        style={{
                            width: "48px",
                            position: "relative",
                            borderRadius: "50px",
                            height: "48px",
                            lineHeight: "20px",
                            fontWeight: "500",
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                            cursor: "pointer",
                            backgroundColor:
                                page === nember ? "#0a65cc" : "#fff",
                            color: page === nember ? "#fff" : "#000",
                            border: page === nember ? "none" : "1px solid #ddd",
                        }}
                    >
                        {nember}
                    </div>
                ))}
                <div
                    onClick={handleNextPage}
                    style={{
                        borderRadius: "84px",
                        backgroundColor: "#ecf9ea",
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "flex-start",
                        justifyContent: "flex-start",
                        padding: "12px",
                        cursor: "pointer",
                    }}
                >
                    <img
                        style={{
                            width: "24px",
                            height: "24px",
                            overflow: "hidden",
                            flexShrink: "0",
                        }}
                        alt="Next"
                        src="/arrow-forward2.svg"
                    />
                </div>
            </div>
        </div>
    );
};

export default EmployeurList;
