import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getGroups } from "../redux/groupActions";

const TeamList = () => {
    const dispatch = useDispatch();
    const groups = useSelector((state) => state.data?.groups || []); // التحقق من وجود 'groups'
    const [page, setPage] = useState(1);



    useEffect(() => {
        dispatch(getGroups(page));
    }, [page, dispatch]);

    const handleNextPage = () => {
        setPage(page + 1);
    };

    const handlePrevPage = () => {
        if (page > 1) {
            setPage(page - 1);
        }
    };

    if (!groups) {
        return <div>Loading...</div>; // أو يمكنك استخدام مؤشر تحميل أفضل
    }

    return (
        <div>
            <h1>المجموعات</h1>
            <ul>
                {groups.map((group) => (
                    <li key={group.id}>
                        <h3>{group.name}</h3>
                        <p>{group.description}</p>
                    </li>
                ))}
            </ul>
            <button onClick={handlePrevPage} disabled={page === 1}>
                الصفحة السابقة
            </button>
            <button onClick={handleNextPage}>الصفحة التالية</button>
        </div>
    );
};

export default TeamList;
