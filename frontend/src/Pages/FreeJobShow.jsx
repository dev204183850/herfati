/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

const FreeJobShow = () => {
        const navigate = useNavigate();

        useEffect(() => {
            if (!window.localStorage.getItem("Acces_Token")) {
                navigate("/login");
            }
        }, []);
    return <>page job show</>;
};

export default FreeJobShow;
