import { createBrowserRouter } from "react-router-dom";
import EmployeurList from "../Pages/EmployeurList";
import HomePage from "../Pages/HomePage";
import FreeJobList from "../Pages/FreeJobList";
import CompanyList from "../Pages/CompanyList";
import TeamList from "../Pages/TeamList";
import Chat from "../Pages/Chat";
import Dashboard from "../Pages/Dashboard";
import FreeJobShow from "../Pages/FreeJobShow";
import CompanyShow from "../Pages/CompanyShow";
import EmployeurShow from "../Pages/EmployeurShow";
import TeamShow from "../Pages/TeamShow";
import PageNotFound from "../Pages/PageNotFound";
import AuthLayout from "../layouts/authLayout";
import GeustLayout from "../layouts/geustLayout";
import DashboardLayout from "../layouts/dashboardLayout";
import LoginRegesterLayout from "../layouts/loginRegesterLayout";
export const router = createBrowserRouter([
    {
        element: <GeustLayout />,
        children: [
            {
                path: "/",
                element: <HomePage type={"home"} />,
            },
        ],
    },
    {
        element: <AuthLayout />,
        children: [
            {
                path: "/freejobs",
                element: <FreeJobList />,
            },
            {
                path: "/freejobs/{id}",
                element: <FreeJobShow data={"id"} />,
            },
            {
                path: "/candidats",
                element: <EmployeurList />,
            },
            {
                path: "/candidats/{id}",
                element: <EmployeurShow data={"id"} />,
            },
            {
                path: "/companys",
                element: <CompanyList />,
            },
            {
                path: "/companys/{id}",
                element: <CompanyShow data={"id"} />,
            },
            {
                path: "/teams",
                element: <TeamList />,
            },
            {
                path: "/teams/{id}",
                element: <TeamShow data={"id"} />,
            },
            {
                path: "/chat",
                element: <Chat />,
            },
        ],
    },

    {
        element: <DashboardLayout />,
        children: [
            {
                path: "/dashboard",
                element: <Dashboard />,
            },
        ],
    },
    {
        element: <LoginRegesterLayout />,
        children: [
            {
                path: "/login",
                element: <HomePage type={"login"} />,
            },
            {
                path: "/register",
                element: <HomePage type={"register"} />,
            },
            {
                path: "*",
                element: <PageNotFound />,
            },
        ],
    },
]);
