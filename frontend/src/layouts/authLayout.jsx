import { Outlet } from "react-router-dom";
import Footer from "../Compenents/footer";
import Navigation from "../Compenents/navigation";

export default function AuthLayout() {
    return <>
        <Navigation type={2}/>
        <main style={{height:"auto"}}>
            <Outlet />
        </main>
        <Footer />
    </>;
}


