import { Outlet } from "react-router-dom";
import Footer from "../Compenents/footer";
import Navigation from "../Compenents/navigation";

export default function LoginRegesterLayout() {
    return (
        <>
            <Navigation type={4} />
            <main style={{ height: "auto" }}>
                <Outlet />
            </main>
            <Footer />
        </>
    );
}


