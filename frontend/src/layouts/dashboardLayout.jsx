import { Outlet } from "react-router-dom";
import Footer from "../Compenents/footer";
import Navigation from "../Compenents/navigation";

export default function DashboardLayout() {
    return (
        <>
            <Navigation type={3} />
            <main style={{ height: "auto"}}>
                <Outlet />
            </main>
            <Footer />
        </>
    );
}


