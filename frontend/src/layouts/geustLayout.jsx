import { Outlet } from "react-router-dom";
import Footer from "../Compenents/footer";
import Navigation from "../Compenents/navigation";

export default function GeustLayout() {
    return <>
        <Navigation type={1}/>
        <main style={{height:"auto" }}>
            <Outlet />
        </main>
        <Footer />
    </>;
}


