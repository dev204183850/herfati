import Instance from "../axiosConfig";
export const fetchGroups = async (page) => {
    try {
        const response = await Instance.get(`/groups/?page=${page}`);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};
export const fetchGroupById = async (id) => {
    try {
        const response = await Instance.get(`/groups/${id}`);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const createGroup = async (groupData) => {
    try {
        const response = await Instance.post("/groups", groupData, {
            headers: {
                "Content-Type": "multipart/form-data",
            },
        });
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};
export const updateGroup = async (id, groupData) => {
    try {
        const response = await Instance.put(`/groups/${id}`, groupData, {
            headers: {
                "Content-Type": "multipart/form-data",
            },
        });
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};
export const deleteGroup = async (id) => {
    try {
        const response = await Instance.delete(`/groups/${id}`);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const inviteMember = async (id, userId) => {
    try {
        const response = await Instance.post(`/groups/${id}/invite`, {
            user_id: userId,
        });
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const acceptInvite = async (id, userId) => {
    try {
        const response = await Instance.post(`/groups/${id}/accept`, {
            user_id: userId,
        });
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};
