import axios from "axios";

export const fetchUsers = async (page) => {
    try {
        const response = await axios.get(`/messages?page=${page}`);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const fetchMessages = async (userId, page) => {
    try {
        const response = await axios.get(`/messages/${userId}?page=${page}`);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const createMessage = async (messageData) => {
    try {
        const response = await axios.post(`/messages`, messageData);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};
