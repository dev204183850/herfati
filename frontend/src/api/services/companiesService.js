import Instance from "../axiosConfig";

export const fetchCompanies = async (page) => {
    try {
        const response = await Instance.get(`/companies?page=${page}`);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const fetchCompanyById = async (id) => {
    try {
        const response = await Instance.get(`/companies/${id}`);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const createCompany = async (companyData) => {
    try {
        const response = await Instance.post("/companies", companyData, {
            headers: {
                "Content-Type": "multipart/form-data",
            },
        });
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const updateCompany = async (id, companyData) => {
    try {
        const response = await Instance.put(`/companies/${id}`, companyData, {
            headers: {
                "Content-Type": "multipart/form-data",
            },
        });
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const deleteCompany = async (id) => {
    try {
        const response = await Instance.delete(`/companies/${id}`);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};
