import Instance from "../axiosConfig";

export const fetchNotifications = async (page) => {
    try {
        const response = await Instance.get(`/notifications?page=${page}`);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const markNotificationAsRead = async (id) => {
    try {
        const response = await Instance.post(`/notifications/${id}/read`);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};
// mgadich nkhdem biha
export const getNotificationById = async (id) => {
    try {
        const response = await Instance.get(`notification/${id}`);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};
