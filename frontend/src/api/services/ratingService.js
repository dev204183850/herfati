import Instance from "axios";

export const fetchAverageRating = async (rateableId, rateableType) => {
    try {
        const response= await Instance.get(`/ratings/average?rateable_id=${rateableId}&rateable_type=${rateableType}`);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};



export const createRating = async (ratingData) => {
    try {
        const response = await Instance.post('/ratings', ratingData);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};
