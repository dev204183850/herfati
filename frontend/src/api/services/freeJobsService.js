import Instance from "../axiosConfig";

export const fetchJobs = async (page) => {
    try {
    const response = await Instance.get(`/free-jobs/?page=${page}`);
    return response.data;
    } catch (error) {
        throw error.response.data;
    }
};
export const fetchJobById = async (companyId) => {
    try {
    const response = await Instance.get(`/free-jobs/${companyId}`);
    return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const createJob = async (jobData) => {
    try {
    const response = await Instance.post("/free-jobs", jobData, {
        headers: {
            "Content-Type": "multipart/form-data",
        },
    });
    return response.data;
    } catch (error) {
        throw error.response.data;
    }
};
export const updateJob = async (id, jobData) => {
    try {
    const response = await Instance.put(`/free-jobs/${id}`, jobData, {
        headers: {
            "Content-Type": "multipart/form-data",
        },
    });
    return response.data;
    } catch (error) {
        throw error.response.data;
    }
};
export const deleteJob = async (id) => {
    try {
    const response = await Instance.delete(`/free-jobs/${id}`);
    return response.data;
    } catch (error) {
        throw error.response.data;
    }
};
