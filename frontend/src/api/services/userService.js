import Instance from "../axiosConfig";

export const login = async (data) => {
    try {
        const response = await Instance.post("/login", data);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const register = async (data) => {
    try {
        const response = await Instance.post("/register", data);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const logout = async () => {
    try {
        await Instance.post("/logout");
    } catch (error) {
        throw error.response.data;
    }
};
export const getProfile = async () => {
    try {
        const response = await Instance.get("/user");
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const updateProfile = async (userId, userData) => {
    try {
        const response = await Instance.put(`/user/${userId}`, userData);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const fetchUsers = async (page) => {
    try {
        const response = await Instance.get(`/users/?page=${page}`);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const fetchUserById = async (id) => {
    try {
        const response = await Instance.get(`/users/${id}`);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const fetchUserContent = async (id) => {
    try {
        const response = await Instance.get(`/users/${id}/content`);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};
