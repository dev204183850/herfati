<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Requests\StoreCompanyRequest;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $companies = Company::orderBy('created_at', 'desc')->paginate(15);
        return response()->json($companies);
        //return view('companies.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCompanyRequest $request)
    {
        $validated = $request->validated();
        
        if ($request->hasFile('company_image')) {
            $validated['company_image'] = $request->file('company_image')->store('company_images', 'public');
        }

        $company = Company::create($validated);

        return response()->json($company, 201);
        // return redirect()->route('companies.index')->with('success', 'Company created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Company $company)
    {
        return response()->json($company);
        //return view('companies.show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Company $company)
    {
        //return view('companies.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreCompanyRequest $request, Company $company)
    {
        $validated = $request->validated();

        if ($request->hasFile('company_image')) {
            if ($company->company_image) {
                Storage::disk('public')->delete($company->company_image);
            }
            $validated['company_image'] = $request->file('company_image')->store('company_images', 'public');
        }

        $company->update($validated);

        return response()->json($company);
        //return redirect()->route('companies.index')->with('success', 'Company updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Company $company)
    {
        if ($company->company_image) {
            Storage::disk('public')->delete($company->company_image);
        }

        $company->delete();

        return response()->json(null, 204);
        //return redirect()->route('companies.index')->with('success', 'Company deleted successfully.');
    }
}
