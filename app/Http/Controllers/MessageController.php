<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Http\Request;
use App\Http\Requests\StoreMessageRequest;
use Illuminate\Support\Facades\DB;

class MessageController extends Controller
{
    public function index()
    {
        $userId = auth()->id();
        $conversations = DB::table('messages')
            ->select('receiver_id', 'message', 'read_at',
                     DB::raw('MAX(created_at) as last_message_time'))
            ->where('sender_id', $userId)
            ->orWhere('receiver_id', $userId)
            ->groupBy('receiver_id')
            ->orderBy('last_message_time', 'desc')
            ->paginate(10);

        return response()->json($conversations);
    }
    public function show($userId)
    {
        $authUserId = auth()->id();
        $messages = Message::where(function ($query) use ($authUserId, $userId) {
            $query->where('sender_id', $authUserId)
                ->where('receiver_id', $userId);
        })
            ->orWhere(function ($query) use ($authUserId, $userId) {
                $query->where('sender_id', $userId)
                    ->where('receiver_id', $authUserId);
            })
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        return response()->json($messages);
    }
    /**
     * Display a listing of the resource.
     */
    /*public function index()
    {
        $messages = Message::all();
        return response()->json($messages);
        //return view('messages.index', compact('messages'));
    }

    /**
     * Show the form for creating a new resource.
     */
    /*public function create()
    {
        //return view('messages.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreMessageRequest $request)
    {
        $validated = $request->validated();
        $message = Message::create($validated);
        return response()->json($message, 201);
        //return redirect()->route('messages.index')->with('success', 'Message sent successfully.');
    }

    /**
     * Display the specified resource.
     */
    /*public function show(Message $message)
    {
        return response()->json($message);
        //return view('messages.show', compact('message'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    /*public function edit(Message $message)
    {
        //return view('messages.edit', compact('message'));
    }

    /**
     * Update the specified resource in storage.
     */
    /*public function update(StoreMessageRequest $request, Message $message)
    {
        $validated = $request->validated();
        $message->update($validated);
        return response()->json($message);
        //return redirect()->route('messages.index')->with('success', 'Message updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    /*public function destroy(Message $message)
    {
        $message->delete();
        return response()->json(null, 204);
        //return redirect()->route('messages.index')->with('success', 'Message deleted successfully.');
    }*/
}
