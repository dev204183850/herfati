<?php

namespace App\Http\Controllers;

use App\Models\Rating;
use Illuminate\Http\Request;
use App\Http\Requests\StoreRatingRequest;

class RatingController extends Controller
{
    public function averageRating(Request $request)
    {
        $rateableId = $request->input('rateable_id');
        $rateableType = $request->input('rateable_type');

        $ratings = Rating::where('rateable_id', $rateableId)
                         ->where('rateable_type', $rateableType)
                         ->orderBy('created_at', 'desc')
                         ->avg('rating');

        return response()->json($ratings);
    }
    /**
     * Display a listing of the resource.
     */
    /*public function index()
    {
        $ratings = Rating::all();
        return response()->json($ratings);
        //return view('ratings.index', compact('ratings'));
    }

    /**
     * Show the form for creating a new resource.
     */
    /*public function create()
    {
        //return view('ratings.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRatingRequest $request)
    {
        $validated = $request->validated();
        $rating = Rating::create($validated);
        return response()->json($rating, 201);
        //return redirect()->route('ratings.index')->with('success', 'Rating created successfully.');
    }

    /**
     * Display the specified resource.
     */
    /*public function show(Rating $rating)
    {
        return response()->json($rating);
        //return view('ratings.show', compact('rating'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    /*public function edit(Rating $rating)
    {
        //return view('ratings.edit', compact('rating'));
    }

    /**
     * Update the specified resource in storage.
     */
    /*public function update(StoreRatingRequest $request, Rating $rating)
    {
        $validated = $request->validated();
        $rating->update($validated);
        return response()->json($rating);
        //return redirect()->route('ratings.index')->with('success', 'Rating updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    /*()public function destroy(Rating $rating)
    {
        $rating->delete();
        return response()->json(null, 204);
        //return redirect()->route('ratings.index')->with('success', 'Rating deleted successfully.');
    }*/
}
