<?php

namespace App\Http\Controllers;

use App\Models\GroupMember;
use Illuminate\Http\Request;
use App\Http\Requests\StoreGroupMemberRequest;

class GroupMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $groupMembers = GroupMember::all();
        return response()->json($groupMembers);
        //return view('group_members.index', compact('groupMembers'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //return view('group_members.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreGroupMemberRequest $request)
    {
        $validated = $request->validated();
        $groupMember = GroupMember::create($validated);
        return response()->json($groupMember, 201);
        //return redirect()->route('group_members.index')->with('success', 'Group Member created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(GroupMember $groupMember)
    {
        return response()->json($groupMember);
        //return view('group_members.show', compact('groupMember'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(GroupMember $groupMember)
    {
        //return view('group_members.edit', compact('groupMember'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreGroupMemberRequest $request, GroupMember $groupMember)
    {
        $validated = $request->validated();
        $groupMember->update($validated);
        return response()->json($groupMember);
        //return redirect()->route('group_members.index')->with('success', 'Group Member updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(GroupMember $groupMember)
    {
        $groupMember->delete();
        return response()->json(null, 204);
        //return redirect()->route('group_members.index')->with('success', 'Group Member deleted successfully.');
    }

    public function getGroupMembersByGroupId($groupId)
    {
        $groupMembers = GroupMember::where('group_id', $groupId)->get();
        return response()->json($groupMembers);
    }
}
