<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreNotificationRequest;
use App\Models\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    /*public function show(Notification $notification)
    {
        return response()->json($notification);
        // return view('notifications.show', compact('notification'));
    }*/
    public function index()
    {
        $user = auth()->id();
        $notifications = Notification::where('user_id', $user)
                                     ->orderBy('created_at', 'desc')
                                     ->paginate(10);

        return response()->json($notifications);
    }
    // عرض تنبيه معين حسب المعرف
    public function show($id)
    {
        $notification = Notification::where('user_id', auth()->id())
            ->findOrFail($id);

        return response()->json($notification);
    }

    // تحديد التنبيه كمقروء
    public function markAsRead($id)
    {
        $notification = Notification::where('user_id', auth()->id())
            ->findOrFail($id);

        $notification->update(['read_at' => now()]);

        return response()->json($notification);
    }
    /**
     * Display a listing of the resource.
     */
    /*public function index()
    {
        $notifications = Notification::all();
        return response()->json($notifications);
        //return view('notifications.index', compact('notifications'));
    }

    /**
     * Show the form for creating a new resource.
     */
    /*public function create()
    {
        //return view('notifications.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    /*public function store(StoreNotificationRequest $request)
    {
        $validated = $request->validated();
        $notification = Notification::create($validated);
        return response()->json($notification, 201);
        //return redirect()->route('notifications.index')->with('success', 'Notification created successfully.');
    }*/

    /**
     * Display the specified resource.
     */


    /**
     * Show the form for editing the specified resource.
     */
    /*public function edit(Notification $notification)
    {
        //return view('notifications.edit', compact('notification'));
    }*/

    /**
     * Update the specified resource in storage.
     */
    /*public function update(StoreNotificationRequest $request, Notification $notification)
    {
        $validated = $request->validated();
        $notification->update($validated);
        return response()->json($notification);
        //return redirect()->route('notifications.index')->with('success', 'Notification updated successfully.');
    }*/

    /**
     * Remove the specified resource from storage.
     */
    /*public function destroy(Notification $notification)
    {
        $notification->delete();
        return response()->json(null, 204);
        //return redirect()->route('notifications.index')->with('success', 'Notification deleted successfully.');
    }*/


}
