<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreFreeJobRequest;
use App\Models\FreeJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FreeJobController extends Controller
{
    public function index(Request $request)
    {
        $jobs = FreeJob::orderBy('created_at', 'desc')->paginate(15);

        return response()->json($jobs);
    }
    /**
     * Display a listing of the resource.
     */
   /* public function index()
    {
        $freeJobs = FreeJob::all();
        return response()->json($freeJobs);
        //return view('freejobs.index', compact('freeJobs'));
    }*/

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //return view('freejobs.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreFreeJobRequest $request)
    {
        $validated = $request->validated();

        if ($request->hasFile('job_image')) {
            $validated['job_image'] = $request->file('job_image')->store('job_images', 'public');
        }

        $freeJob = FreeJob::create($validated);

        return response()->json($freeJob, 201);
        //return redirect()->route('freejobs.index')->with('success', 'FreeJob created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(FreeJob $freeJob)
    {
        return response()->json($freeJob);
        //return view('freejobs.show', compact('freeJob'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(FreeJob $freeJob)
    {
        //return view('freejobs.edit', compact('freeJob'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreFreeJobRequest $request, FreeJob $freeJob)
    {
        $validated = $request->validated();

        if ($request->hasFile('job_image')) {
            if ($freeJob->job_image) {
                Storage::disk('public')->delete($freeJob->job_image);
            }
            $validated['job_image'] = $request->file('job_image')->store('job_images', 'public');
        }

        $freeJob->update($validated);

        return response()->json($freeJob);
        //return redirect()->route('freejobs.index')->with('success', 'FreeJob updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(FreeJob $freeJob)
    {
        if ($freeJob->job_image) {
            Storage::disk('public')->delete($freeJob->job_image);
        }

        $freeJob->delete();

        return response()->json(null, 204);
        //return redirect()->route('freejobs.index')->with('success', 'FreeJob deleted successfully.');
    }
}
