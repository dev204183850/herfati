<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Models\Company;
use App\Models\FreeJob;
use App\Models\Group;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::orderBy('created_at', 'desc')->paginate(15);

        return response()->json($users);
    }
    /**
     * Display a listing of the resource.
     */
    /*public function index()
    {
        $users = User::all();
        //dd(response()->json($employeurs));
        return response()->json($users);
        //return view('employeurs.index', compact('employeurs'));
    }*/

    /**
     * Show the form for creating a new resource.
     */
    /*public function create()
    {
        //return view('employeurs.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    /*public function store(StoreUserRequest $request)
    {
        $validated = $request->validated();
        $validated['password'] = Hash::make($request->password);

        if ($request->hasFile('profile_image')) {
            $validated['profile_image'] = $request->file('profile_image')->store('profile_images', 'public');
        }

        if ($request->hasFile('cv_file')) {
            $validated['cv_file'] = $request->file('cv_file')->store('cv_files', 'public');
        }

        $employeur = User::create($validated);

        return response()->json($employeur, 201);
        //return redirect()->route('employeurs.index')->with('success', 'Employeur created successfully.');
    }*/

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return response()->json($user);
    }
    /*public function show(User $user)
    {
        //dd(response()->json($user));
        return response()->json($user);
        //return view('employeurs.show', compact('employeur'));
    }*/

    /**
     * Show the form for editing the specified resource.
     */
    /*public function edit(User $employeur)
    {
        //return view('employeurs.show', compact('employeur'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreUserRequest $request, User $user)
    {
        $validated = $request->validated();


        if ($request->filled('password')) {
            $validated['password'] = Hash::make($request->password);
        }

        if ($request->hasFile('profile_image')) {
            if ($user->profile_image) {
                Storage::disk('public')->delete($user->profile_image);
            }
            $validated['profile_image'] = $request->file('profile_image')->store('profile_images', 'public');
        }

        if ($request->hasFile('cv_file')) {
            if ($user->cv_file) {
                Storage::disk('public')->delete($user->cv_file);
            }
            $validated['cv_file'] = $request->file('cv_file')->store('cv_files', 'public');
        }

        $user->update($validated);

        return response()->json($user);
        //return redirect()->route('employeurs.index')->with('success', 'Employeur updated successfully.');
    }
    public function userCreatedContent($id)
    {
        $user = User::findOrFail($id);
        $jobs = FreeJob::where('user_id', $id)->get();
        $companies = Company::where('user_id', $id)->get();
        $groups = Group::where('creator_id', $id)->get();

        return response()->json([
            'jobs' => $jobs,
            'companies' => $companies,
            'groups' => $groups,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    /*public function destroy(User $employeur)
    {
        if ($employeur->profile_image) {
            Storage::disk('public')->delete($employeur->profile_image);
        }
        if ($employeur->cv_file) {
            Storage::disk('public')->delete($employeur->cv_file);
        }

        $employeur->delete();

        return response()->json(null, 204);
        //return redirect()->route('employeurs.index')->with('success', 'Employeur deleted successfully.');
    }*/
}
