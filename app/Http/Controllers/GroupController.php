<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreNotificationRequest;
use App\Http\Requests\StoreUserRequest;
use App\Models\Group;
use App\Http\Requests\StoreGroupRequest;
use App\Models\GroupMember;
use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GroupController extends Controller
{
    public function inviteUserToGroup(Request $request)
    {
        $groupId = $request->input('group_id');
        $userId = $request->input('user_id');

        // إرسال دعوة
        Notification::create([
            'user_id' => $userId,
            'message' => "تمت دعوتك للانضمام إلى المجموعة.",
            'type' => 'group_invitation'
        ]);

        return response()->json(['message' => 'Invitation sent']);
    }

    public function acceptGroupInvitation(Request $request)
    {
        $groupId = $request->input('group_id');
        $userId = $request->user()->id;

        // إضافة المستخدم إلى المجموعة
        GroupMember::create([
            'group_id' => $groupId,
            'user_id' => $userId,
            'role' => 'member'
        ]);

        // تحديث حالة التنبيه
        Notification::where('user_id', $userId)
            ->where('message', 'تمت دعوتك للانضمام إلى المجموعة.')
            ->update(['read_at' => now()]);

        return response()->json(['message' => 'User added to group']);
    }

    public function index(Request $request)
    {
        $groups = Group::orderBy('created_at', 'desc')->paginate(8);

        return response()->json($groups);
    }
    /**
     * Display a listing of the resource.
     */
    /*public function index()
    {
        $groups = Group::all();
        return response()->json($groups);
        //return view('groups.index', compact('groups'));
    }*/

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //return view('groups.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreGroupRequest $request)
    {
        $validated = $request->validated();

        if ($request->hasFile('group_image')) {
            $validated['group_image'] = $request->file('group_image')->store('group_images', 'public');
        }

        $group = Group::create($validated);

        return response()->json($group, 201);
        //return redirect()->route('groups.index')->with('success', 'Group created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Group $group)
    {
        return response()->json($group);
        //return view('groups.show', compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Group $group)
    {
        //return view('groups.edit', compact('group'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreGroupRequest $request, Group $group)
    {
        $validated = $request->validated();

        if ($request->hasFile('group_image')) {
            if ($group->group_image) {
                Storage::disk('public')->delete($group->group_image);
            }
            $validated['group_image'] = $request->file('group_image')->store('group_images', 'public');
        }

        $group->update($validated);

        return response()->json($group);
        // return redirect()->route('groups.index')->with('success', 'Group updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Group $group)
    {
        if ($group->group_image) {
            Storage::disk('public')->delete($group->group_image);
        }

        $group->delete();

        return response()->json(null, 204);
        //return redirect()->route('groups.index')->with('success', 'Group deleted successfully.');
    }
    // إرسال دعوة لمستخدم للانضمام للمجموعة
    public function inviteMember(StoreUserRequest $request, $groupId)
    {
        $validated = $request->validated();



            Notification::create([
            'user_id' => $validated['user_id'],
            'message' => "You have been invited to join the group with ID: $groupId",
            'type' => 'group_invitation'
        ]);

        return response()->json(['message' => 'Invitation sent successfully']);
    }

    // قبول دعوة للانضمام للمجموعة
    public function acceptInvite(StoreNotificationRequest $request, $groupId)
    {
        $validated = $request->validated();



        GroupMember::create([
            'group_id' => $groupId,
            'user_id' => $validated['user_id'],
            'role' => 'member'
        ]);

        Notification::where('user_id', $validated['user_id'])
            ->where('message', "You have been invited to join the group with ID: $groupId")
            ->update(['read_at' => now()]);

        return response()->json(['message' => 'You have joined the group successfully']);
    }
}
