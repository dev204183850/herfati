<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreFreeJobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'user_id' => 'required|exists:users,id',
            'job_image' => 'nullable|mimes:png,jpg,jpeg,svg|image|max:2048',
            'location' => 'required|string|max:255',
            'salary' => 'required|numeric|min:0',
            'employment_type' => 'required|string|in:full-time,part-time,contract',
            'experience_required' => 'nullable|string',
            'education_required' => 'nullable|string',
            'skills_required' => 'nullable|string',
            'language_requirements' => 'nullable|string',
        ];
    }
}
