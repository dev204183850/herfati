<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'description' => 'nullable|string',
            'creator_id' => 'required|exists:users,id',
            'group_image' => 'nullable|mimes:png,jpg,jpeg,svg|image|max:2048',
            'location' => 'required|string|max:255',
            'field' => 'required|string|max:255',
            'requirements' => 'nullable|string',
            'perks' => 'nullable|string',
        ];
    }
}
