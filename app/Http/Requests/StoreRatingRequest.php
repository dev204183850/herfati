<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRatingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'rateable_id' => 'required|integer',
            'rateable_type' => 'required|string|in:App\Models\user,App\Models\Company,App\Models\Group',
            'rating' => 'required|integer|min:1|max:5',
            'comment' => 'nullable|string',
            'rated_by' => 'required|exists:users,id',
        ];
    }
}
