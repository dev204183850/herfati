<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FreeJob extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'description','user_id',
        'job_image', 'location', 'salary',
        'employment_type', 'experience_required',
        'education_required', 'skills_required',
        'language_requirements'
    ];

    // Relationships
    
}
