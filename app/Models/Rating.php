<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    use HasFactory;

    protected $fillable = [
        'rateable_id', 'rateable_type', 'rating', 'comment', 'rated_by'
    ];

    // Relationships
    public function rateable()
    {
        return $this->morphTo();
    }

    public function rater()
    {
        return $this->belongsTo(User::class, 'rated_by');
    }
}
