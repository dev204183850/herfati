<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'description','user_id', 'company_image', 'industry', 'location', 'website',
        'contact_email', 'phone_number'
    ];

    // Relationships
    public function freeJobs()
    {
        return $this->hasMany(FreeJob::class);
    }

    public function ratings()
    {
        return $this->morphMany(Rating::class, 'rateable');
    }
}
