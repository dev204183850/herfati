<?php

use App\Http\Controllers\CompanyController;
use App\Http\Controllers\FreeJobController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\GroupMemberController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\RatingController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require __DIR__.'/auth.php';
    Route::resource('users', UserController::class);
    Route::resource('freejobs', FreeJobController::class);
    Route::resource('companies', CompanyController::class);
    Route::resource('groups', GroupController::class);
    Route::resource('groupmembers', GroupMemberController::class);
    Route::resource('messages', MessageController::class);
    Route::resource('notifications', NotificationController::class);
    Route::resource('ratings', RatingController::class);

