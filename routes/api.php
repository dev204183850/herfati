<?php
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\FreeJobController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\GroupMemberController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\RatingController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
require __DIR__.'/auth.php';
Route::middleware(['auth:sanctum'])->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:sanctum')->group(function () {
    Route::apiResource('companies', CompanyController::class);
    Route::apiResource('groups', GroupController::class);
    Route::apiResource('group-members', GroupMemberController::class);
});
Route::apiResource('users', UserController::class);
Route::apiResource('free-jobs', FreeJobController::class);


Route::middleware('auth:sanctum')->group(function () {
    Route::get('/users/{id}/content', [UserController::class, 'userCreatedContent']);
});

Route::middleware('auth:sanctum')->group(function () {
    Route::post('/groups/{id}/invite', [GroupController::class, 'inviteMember']);
    Route::post('/groups/{id}/accept', [GroupController::class, 'acceptInvite']);
});

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/messages', [MessageController::class, 'index']);
    Route::post('/messages', [MessageController::class, 'store']);
    Route::get('/messages/{userId}', [MessageController::class, 'show']);
});

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/notifications', [NotificationController::class, 'index']);
    Route::get('/notifications/{id}', [NotificationController::class, 'show']);
    Route::post('/notifications/{id}/read', [NotificationController::class, 'markAsRead']);
});

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/ratings/average', [RatingController::class, 'averageRating']);
    Route::post('/ratings', [RatingController::class, 'store']);
});
