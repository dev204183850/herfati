<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('free_jobs', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description');
            $table->unsignedBigInteger('user_id');
            $table->string('job_image')->nullable();
            $table->string('location');
            $table->decimal('salary', 10, 2)->nullable();
            $table->string('employment_type');
            $table->text('experience_required')->nullable();
            $table->text('education_required')->nullable();
            $table->text('skills_required')->nullable();
            $table->text('language_requirements')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('free_jobs');
    }
};
