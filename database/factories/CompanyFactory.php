<?php

namespace Database\Factories;

use App\Models\Company;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Company>
 */
class CompanyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Company::class;
    public function definition(): array
    {
        return [
            'name' => $this->faker->company,
            'description' => $this->faker->paragraph,
            'user_id'=>User::factory(),
            'company_image' => $this->faker->imageUrl(),
            'industry' => $this->faker->word,
            'location' => $this->faker->address,
            'website' => $this->faker->url,
            'contact_email' => $this->faker->companyEmail,
            'phone_number' => $this->faker->phoneNumber,
        ];
    }
}
