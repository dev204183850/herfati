<?php

namespace Database\Factories;

use App\Models\FreeJob;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\FreeJob>
 */
class FreeJobFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = FreeJob::class;
    public function definition(): array
    {
        return [
            'title' => $this->faker->jobTitle,
            'description' => $this->faker->paragraph,
            'user_id'=>User::factory(),
            'job_image' => $this->faker->imageUrl(),
            'location' => $this->faker->address,
            'salary' => $this->faker->numberBetween(30000, 100000),
            'employment_type' => $this->faker->randomElement(['full-time', 'part-time', 'contract']),
            'experience_required' => $this->faker->sentence,
            'education_required' => $this->faker->sentence,
            'skills_required' => $this->faker->words(5, true),
            'language_requirements' => $this->faker->words(3, true)
        ];
    }
}
