<?php

namespace Database\Factories;

use App\Models\Employeur;
use App\Models\Rating;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Rating>
 */
class RatingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Rating::class;
    public function definition(): array
    {
        return [
            'rateable_id' => $this->faker->numberBetween(1, 100),
            'rateable_type' => $this->faker->randomElement(['App\Models\Employeur', 'App\Models\Company', 'App\Models\Group']),
            'rating' => $this->faker->numberBetween(1, 5),
            'comment' => $this->faker->sentence,
            'rated_by' => User::factory(),
        ];
    }
}
