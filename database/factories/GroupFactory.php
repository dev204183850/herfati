<?php

namespace Database\Factories;

use App\Models\Group;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Group>
 */
class GroupFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Group::class;
    public function definition(): array
    {
        return [
            'name' => $this->faker->word,
            'description' => $this->faker->paragraph,
            'creator_id' => User::factory(),
            'group_image' => $this->faker->imageUrl(),
            'location' => $this->faker->address,
            'field' => $this->faker->word,
            'requirements' => $this->faker->sentence,
            'perks' => $this->faker->sentence
        ];
    }
}
