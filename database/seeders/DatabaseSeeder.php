<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            CompanySeeder::class,
            GroupSeeder::class,
            GroupMemberSeeder::class,
            FreeJobSeeder::class,
            MessageSeeder::class,
            NotificationSeeder::class,
            RatingSeeder::class,
        ]);
        \App\Models\User::factory(100)->create();

        \App\Models\User::factory()->create([
            'name' => 'fouad abbassi',
            'email' => 'fouad@abbassi.com',
            'password' => bcrypt('fouad@abbassi.com'),
        ]);
    }
}
