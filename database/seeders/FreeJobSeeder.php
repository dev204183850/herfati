<?php

namespace Database\Seeders;

use App\Models\FreeJob;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FreeJobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        FreeJob::factory()->count(50)->create();
    }
}
